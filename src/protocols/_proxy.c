#include "m.h"
#include "_proxy.h"


base_ons_connection *_onsCreateProxy(char const *params_)
{
    char const *at_ch = strchr(params_, '@');
    char const *colon_ch = strchr(params_, ':');
    if(at_ch == NULL || at_ch == params_) return NULL;
    if(colon_ch < at_ch) return NULL;
    
    base_ons_connection *proxy = onsCreateConnection(at_ch + 1, 0 /*no check api*/);
    if(proxy == NULL) return NULL;
    
    char storage[at_ch - params_ + 1];
    memcpy(storage, params_, at_ch - params_);
    storage[at_ch - params_] = '\0';
    
    t_minson_object *rq = minsonCreateString("connect", storage);
    t_minson_object *rs = onsConnectionAdapter(proxy, rq);
    minsonDelete(rq);
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    if(r == 0)
    {
        onsDeleteConnection(proxy); return NULL;
    }
    return proxy;
}
