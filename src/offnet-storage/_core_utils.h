#ifndef __OFFNET_STORAGE_CORE_UTILS_H
#define __OFFNET_STORAGE_CORE_UTILS_H

#include <minson.h>
#include <stdatomic.h>


char *_dirdup(char const *str_);

int64_t _current_timestamp();

char *_generate_member_name();

// ---

void _lock(atomic_flag volatile *mutex_);
void _unlock(atomic_flag volatile *mutex_);

int /*nix*/ _file_lock(char const *base_dir_, char const *file_);
void _file_unlock(int fd_, char const *base_dir_, char const *file_);

// ---

t_minson_object *_load(char const *base_dir_, char const *category_,
                       char const *entry_);
void _save(char const *base_dir_, char const *category_,
           char const *entry_, t_minson_object *object_);
void _delete(char const *base_dir_, char const *category_,
             char const *entry_);


#endif // __OFFNET_STORAGE_CORE_UTILS_H
