#include "offnet-storage.hpp"
#include "test.h"


int main(int argc, char *const *argv)
{
    _TEST_(argc == 3);
    try
    {
        ons::connection Storage1 = ons::connection(argv[1]);
        ons::connection Storage2 = ons::connection(argv[2]);
        
        _TEST_(ons::synchronization(Storage1, Storage2));
    }
    catch(ons::url_error &)
    {
        _FAIL_("wrong URL's!");
    }
    _ENDTEST_;
}
