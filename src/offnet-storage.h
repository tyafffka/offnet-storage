#include "offnet-storage/config.h"
#include "offnet-storage/core.h"
#include "offnet-storage/protocols.h"
#include "offnet-storage/interface.h"


/* Возвращает объект соединения, связанный с объектом реализации напрямую.
 * Объект следует удалять с помощью `onsDeleteConnection`. */
base_ons_connection *_onsCreateBaseDirect(t_ons_core *core_);
