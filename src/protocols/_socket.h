#ifndef __OFFNET_STORAGE_SOCKET_PROTOCOL_H
#define __OFFNET_STORAGE_SOCKET_PROTOCOL_H

#include "offnet-storage/protocols.h"


typedef struct _t_ons_socket_connection
{
    base_ons_connection h;
    int _connection_fd;
}
_t_ons_socket_connection;


base_ons_connection *_onsCreateTCP(char const *params_);
#ifndef _WIN32
base_ons_connection *_onsCreateUDS(char const *params_);
#endif

// --- *** ---


typedef struct _t_ons_socket_server
{
    base_ons_server h;
    int _server_fd;
}
_t_ons_socket_server;


base_ons_server *_onsCreateTCPServer(t_minson_array *params_);
#ifndef _WIN32
base_ons_server *_onsCreateUDSServer(t_minson_array *params_);
#endif

// --- *** ---


typedef struct _t_ons_socket_client
{
    base_ons_client h;
    int _client_fd;
    uint32_t ip;
    uint16_t port;
}
_t_ons_socket_client;


#endif // __OFFNET_STORAGE_SOCKET_PROTOCOL_H
