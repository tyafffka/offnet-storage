#include "m.h"
#include "offnet-storage.h"
#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
#ifdef HAVE_READLINE
# include <readline/readline.h>
# include <readline/history.h>
#endif


static void __print_minson(t_minson_object *obj_, int level_)
{
    for(int tab = 0; tab < level_; ++tab) printf("  ");
    
    if(obj_->name != NULL) printf("%s ", obj_->name);
    
    switch(obj_->type)
    {
    case C_MINSON_ARRAY:
    {
        printf("{\n");
        
        for(t_minson_iter iter = minsonFirst(m_pcast(t_minson_array *, obj_));
            minsonNotEnd(iter); minsonNext(iter))
        {
            __print_minson(minsonIterObject(iter), level_ + 1);
        }
        
        for(int tab = 0; tab < level_; ++tab) printf("  ");
        printf("}\n");
        return;
    }
    case C_MINSON_NUMBER:
    {
        int64_t value = m_pcast(t_minson_number *, obj_)->value;
        printf("(%ld)\n", value);
        return;
    }
    default:
    {
        char const *old_name = obj_->name; obj_->name = NULL;
        minsonSerializeStream(obj_, stdout);
        obj_->name = old_name;
        
        printf("\n");
        return;
    }
    }
}

// --- *** ---


#ifndef HAVE_READLINE
# define CMD_BUFFER_SIZE 65000
#endif

static int /*nix*/ Interactive(char const *url_)
{
    if(! isatty(fileno(stdin)) || ! isatty(fileno(stdout)))
    {
        fprintf(stderr, "Error: there is no a terminal!\n");
        return 1;
    }
    
    base_ons_connection *storage = onsCreateConnection(url_, 0 /*no check api*/);
    if(storage == NULL)
    {
        fprintf(stderr, "Error: can't connect to storage '%s'!\n", url_);
        return 2;
    }
    
    size_t prompt_len = strlen(url_) + 4;
    char prompt[prompt_len + 1];
    sprintf(prompt, "(%s)> ", url_);
    
#ifndef HAVE_READLINE
    char cmd_buffer[CMD_BUFFER_SIZE];
#endif
    for(;;)
    {
        fflush(stderr);
#ifdef HAVE_READLINE
        char *cmd_buffer = readline(prompt);
        if(cmd_buffer == NULL)
#else
        fwrite(prompt, 1, prompt_len, stdout);
        if(fgets(cmd_buffer, CMD_BUFFER_SIZE, stdin) == NULL)
#endif
        {
            fprintf(stdout, "\n"); break;
        }
        
        // strip whitespaces
        char const *p_cmd = cmd_buffer;
        while(*p_cmd && isspace(*p_cmd)) ++p_cmd;
        
        char const *p_cmd_end = p_cmd;
        while(*p_cmd_end) ++p_cmd_end;
        while(p_cmd < p_cmd_end && isspace(*(p_cmd_end - 1))) --p_cmd_end;
        
        if(p_cmd == p_cmd_end) continue;
        if(strncmp("exit", p_cmd, p_cmd_end - p_cmd) == 0) break;
        if(strncmp("quit", p_cmd, p_cmd_end - p_cmd) == 0) break;
        
        t_minson_object *command = minsonParse2(p_cmd, p_cmd_end);
#ifdef HAVE_READLINE
        if(p_cmd == cmd_buffer) add_history(cmd_buffer);
        free((void *)cmd_buffer);
#endif
        if(command == NULL)
        {
            fprintf(stderr, "Error: invalid minson!\n"); continue;
        }
        
        t_minson_object *response = onsConnectionAdapter(storage, command);
        minsonDelete(command);
        
        if(response == NULL)
        {
            fprintf(stderr, "Error: invalid response!\n"); continue;
        }
        
        __print_minson(response, 0);
    }
    
    onsDeleteConnection(storage);
    return 0;
}

// --- *** ---


static int /*nix*/ Execute(char const *url_, char const *command_)
{
    int r = 0;
    t_minson_object *command = NULL, *response = NULL;
    base_ons_connection *storage = NULL;
    
    if((command = minsonParse(command_)) == NULL)
    {
        fprintf(stderr, "Error: invalid minson!\n");
        r = 1;
        goto __end;
    }
    
    if((storage = onsCreateConnection(url_, 0 /*no check api*/)) == NULL)
    {
        fprintf(stderr, "Error: can't connect to storage '%s'!\n", url_);
        r = 2;
        goto __end;
    }
    
    if((response = (storage->v_adapter)(storage, command)) == NULL)
    {
        fprintf(stderr, "Error: invalid response!\n");
        r = 3;
        goto __end;
    }
    
    __print_minson(response, 0);
    
__end:
    if(command != NULL) minsonDelete(command);
    if(storage != NULL) onsDeleteConnection(storage);
    return r;
}

// --- *** ---


int /*nix*/ main(int argc, char *const *argv)
{
    if(argc < 2)
    {
        fprintf(stderr,
"Usage: %s <url> [<command>]\n"
"  <url> - storage to connect to\n"
"  <command> - MinSON-command to execute. If not specified, interactive\n"
"    connection has been opened\n",
            argv[0]
        );
        return 0;
    }
    else if(argc < 3)
    {   return Interactive(argv[1]);   }
    else
    {   return Execute(argv[1], argv[2]);   }
}
