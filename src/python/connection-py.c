#include "connection-py.h"


static PyObject *__Connection_new(PyTypeObject *cls, PyObject *args, PyObject *kwargs)
{
    PyObject *self = (cls->tp_alloc)(cls, 0);
    if(self == NULL) return self;
    
    pconn(self)->__p = NULL;
    return self;
}

static int __Connection_init(PyObject *self, PyObject *args, PyObject *kwargs)
{
    char const *url_;
    if(! PyArg_ParseTuple(args, "s", &url_)) return -1;
    
    pconn(self)->__p = onsCreateConnection(url_, 1 /*check api*/);
    if(pconn(self)->__p == NULL)
    {
        PyErr_SetString(ons_URLError, "Can't initialize connection by url!");
        return -1;
    }
    return 0;
}

static void __Connection_delete(PyObject *self)
{
    if(pconn(self)->__p) onsDeleteConnection(pconn(self)->__p);
    
    (Py_TYPE(self)->tp_free)(self);
    return;
}

// ---

static PyObject *__Connection_getCategory(PyObject *self, PyObject *args)
{
    char const *category_;
    if(! PyArg_ParseTuple(args, "s", &category_)) return NULL;
    
    t_minson_array *r = onsGetCategory(pconn(self)->__p, category_);
    if(r == NULL)
    {
        PyErr_SetString(PyExc_KeyError, "Category not found!");
        return NULL;
    }
    return _minson_instance(minsonDuplicate(minsonUpcast(r)), true);
}

static PyObject *__Connection_getCategoryAll(PyObject *self, PyObject *args)
{
    char const *category_;
    if(! PyArg_ParseTuple(args, "s", &category_)) return NULL;
    
    t_minson_array *r = onsGetCategoryAll(pconn(self)->__p, category_);
    if(r == NULL)
    {
        PyErr_SetString(PyExc_KeyError, "Category not found!");
        return NULL;
    }
    return _minson_instance(minsonDuplicate(minsonUpcast(r)), true);
}

static PyObject *__Connection_generate(PyObject *self, PyObject *args)
{
    char const *category_;
    if(! PyArg_ParseTuple(args, "s", &category_)) return NULL;
    
    char const *r = onsGenerateEntry(pconn(self)->__p, category_);
    if(r == NULL)
    {
        PyErr_SetString(PyExc_KeyError, "Category not found!");
        return NULL;
    }
    return PyString_FromString(r);
}

// ---

static PyObject *__Connection_get(PyObject *self, PyObject *args)
{
    char const *category_, *entry_;
    if(! PyArg_ParseTuple(args, "ss", &category_, &entry_)) return NULL;
    
    t_minson_object *r = onsGetEntry(pconn(self)->__p, category_, entry_);
    if(r == NULL)
    {
        PyErr_SetString(PyExc_KeyError, "Entry not found!");
        return NULL;
    }
    return _minson_instance(minsonDuplicate(r), true);
}

static PyObject *__Connection_set(PyObject *self, PyObject *args)
{
    char const *category_;
    PyObject *entry_;
    if(! PyArg_ParseTuple(args, "sO", &category_, &entry_)) return NULL;
    
    if(! PyObject_TypeCheck(entry_, &MinSONObjectClass))
    {
        PyErr_SetString(PyExc_TypeError, "Entry must be of 'MinSONObject' type!");
        return NULL;
    }
    
    if(onsSetEntry(pconn(self)->__p, category_, pobj(entry_)))
        Py_RETURN_TRUE;
    else
        Py_RETURN_FALSE;
}

static PyObject *__Connection_remove(PyObject *self, PyObject *args)
{
    char const *category_, *entry_;
    if(! PyArg_ParseTuple(args, "ss", &category_, &entry_)) return NULL;
    
    if(onsDeleteEntry(pconn(self)->__p, category_, entry_))
        Py_RETURN_TRUE;
    else
        Py_RETURN_FALSE;
}

// ---

static PyObject *__Connection_flush(PyObject *self)
{
    if(onsFlush(pconn(self)->__p))
        Py_RETURN_TRUE;
    else
        Py_RETURN_FALSE;
}

static PyObject *__Connection_reset(PyObject *self)
{
    if(onsReset(pconn(self)->__p))
        Py_RETURN_TRUE;
    else
        Py_RETURN_FALSE;
}

// ---

static PyObject *__Connection_changeMemberName(PyObject *self, PyObject *args)
{
    char const *new_name_;
    if(! PyArg_ParseTuple(args, "s", &new_name_)) return NULL;
    
    if(onsChangeMemberName(pconn(self)->__p, new_name_))
        Py_RETURN_TRUE;
    else
        Py_RETURN_FALSE;
}

// --- *** ---


PyObject *ons_URLError;


static PyMemberDef ConnectionMembers[] = {
    {NULL}
};

static PyMethodDef ConnectionMethods[] = {
    {"getCategory", __Connection_getCategory, METH_VARARGS, "Возвращает массив записей запрошенной категории."},
    {"getCategoryAll", __Connection_getCategoryAll, METH_VARARGS, "Возвращает массив всех записей (+ удалённые) запрошенной категории."},
    {"generate", __Connection_generate, METH_VARARGS, "Создать новую запись в указанной категории с произвольным именем."},
    {"get", __Connection_get, METH_VARARGS, "Возвращает MinSON-объект, сохранённый в указанной записи."},
    {"set", __Connection_set, METH_VARARGS, "Сохранить MinSON-объект как запись в указанной категории."},
    {"remove", __Connection_remove, METH_VARARGS, "Удаляет указанную запись."},
    {"flush", (PyCFunction)__Connection_flush, METH_NOARGS, "Зафиксировать изменения в хранилище."},
    {"reset", (PyCFunction)__Connection_reset, METH_NOARGS, "Отменить последние изменения."},
    {"changeMemberName", __Connection_changeMemberName, METH_VARARGS, "Изменяет имя абонента хранилища."},
    {NULL, NULL, 0, NULL}
};

PyTypeObject ConnectionClass = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "offnet_storage.Connection", /* tp_name */
    sizeof(struct __t_Connection), /* tp_basicsize */
    0,                         /* tp_itemsize */
    __Connection_delete, /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_compare */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT, /* tp_flags */
    "Класс соединения с хранилищем.", /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    ConnectionMethods, /* tp_methods */
    ConnectionMembers, /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    __Connection_init, /* tp_init */
    0,                   /* tp_alloc */
    __Connection_new, /* tp_new */
};
