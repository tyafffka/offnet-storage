#include "connection-py.h"

/* -*- Copied from minson-py -*- */

PyMODINIT_FUNC initminson( void );

// --- *** ---

void _onsInit( void );



static PyObject *__ons_synchronization(PyObject *m, PyObject *args)
{
    PyObject *c1_, *c2_;
    if(! PyArg_ParseTuple(args, "OO", &c1_, &c2_)) return NULL;
    
    if(! PyObject_TypeCheck(c1_, &ConnectionClass) ||
       ! PyObject_TypeCheck(c2_, &ConnectionClass))
    {
        PyErr_SetString(PyExc_TypeError, "Objects must be of 'Connection' type!");
        return NULL;
    }
    
    if(onsSynchronization(pconn(c1_)->__p, pconn(c2_)->__p))
        Py_RETURN_TRUE;
    else
        Py_RETURN_FALSE;
}

// --- *** ---


static PyMethodDef ModuleMethods[] = {
    {"synchronization", __ons_synchronization, METH_VARARGS, "Процедура синхронизации двух хранилищ."},
    {NULL, NULL, 0, NULL}
};


PyMODINIT_FUNC initoffnet_storage( void )
{
    initminson();
    _onsInit();
    
    if(PyType_Ready(&ConnectionClass) < 0) return;
    
    PyObject *m = Py_InitModule3("offnet_storage", ModuleMethods, "Библиотека синхронизируемого хранилища данных.");
    
    // ---
    
    Py_INCREF(&ConnectionClass);
    PyModule_AddObject(m, "Connection", m_pcast(PyObject *, &ConnectionClass));
    
    // ---
    
    ons_URLError = PyErr_NewExceptionWithDoc(
        "offnet_storage.URLError", "Ошибки при инициализации.", NULL, NULL
    );
    Py_INCREF(ons_URLError);
    PyModule_AddObject(m, "URLError", ons_URLError);
    return;
}
