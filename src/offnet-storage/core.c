#include "m.h"
#include "core.h"
#include "_core_impl.h"
#include "_core_utils.h"
#include "offnet-storage/config.h"
#include <stdlib.h>
#include <time.h>

/* Спец-файлы:
 * - matrix.minson:      { <self-name>{ <name> (<time>) ... } <name> (<time>) ... };
 * - table.minson:       { <category>{ <entry> (<time>) ... } ... };
 * - fast-stored.minson: { "<category>" ... };
 * - lock.pid. */


uint8_t /*bool*/ onsCoreInit(t_ons_core *this_, char const *storage_dir_)
{
    this_->__sync_context = NULL;
    this_->__lock_fd = -1;
    this_->__mutex = (_t_ons_mutex)ATOMIC_FLAG_INIT;
    
    this_->_table = this_->_fast_stored = NULL;
    
    if((this_->_storage_directory = _dirdup(storage_dir_)) == NULL)
        return 0;
    
    this_->__lock_fd = _file_lock(this_->_storage_directory, ONS_DATA_LOCK_FILE);
    if(this_->__lock_fd < 0)
    {
        onsCoreClose(this_); return 0;
    }
    
    srand((unsigned)time(NULL)); clock();
    
    // Read table:
    
    t_minson_object *obj = _load(this_->_storage_directory, NULL, ONS_DATA_TABLE);
    this_->_table = minsonCastArray(obj);
    
    if(this_->_table == NULL)
    {
        minsonDelete(obj);
        this_->_table = minsonCreateArray(NULL);
    }
    
    // Read fast-stored categories:
    
    obj = _load(this_->_storage_directory, NULL, ONS_DATA_FAST_STORED);
    t_minson_array *array = minsonCastArray(obj);
    
    this_->_fast_stored = minsonCreateArray(NULL);
    if(array != NULL)
    {
        for(t_minson_iter it = minsonFirst(array); minsonNotEnd(it); minsonNext(it))
        {
            char const *name = minsonIterGetString(it);
            if(name == NULL) continue;
            
            t_minson_object *category = _load(this_->_storage_directory, name, NULL);
            if(category != NULL)
                minsonArrayAdd(this_->_fast_stored, category);
            else
                minsonArrayAddArray(this_->_fast_stored, name);
        }
    }
    
    minsonDelete(obj);
    return 1;
}


void onsCoreClose(t_ons_core *this_)
{
/* { */ _lock(&(this_->__mutex));
    
    if(this_->_fast_stored != NULL)
    {
        for(t_minson_iter it = minsonFirst(this_->_fast_stored);
            minsonNotEnd(it); minsonNext(it))
        {
            if(minsonIterName(it) == NULL) continue;
            
            _save(this_->_storage_directory, minsonIterName(it), NULL,
                  minsonIterObject(it));
        }
        
        minsonDeleteArray(this_->_fast_stored); this_->_fast_stored = NULL;
    }
    
    if(this_->_table != NULL)
    {
        _save(this_->_storage_directory, NULL, ONS_DATA_TABLE,
              minsonUpcast(this_->_table));
        minsonDeleteArray(this_->_table); this_->_table = NULL;
    }
    
    this_->__sync_context = NULL;
    if(this_->__lock_fd >= 0)
    {
        _file_unlock(this_->__lock_fd, this_->_storage_directory, ONS_DATA_LOCK_FILE);
        this_->__lock_fd = -1;
    }
    
    if(this_->_storage_directory != NULL)
    {
        m_delete(this_->_storage_directory); this_->_storage_directory = NULL;
    }
    
/* } */ _unlock(&(this_->__mutex));
    return;
}

// --- *** ---


uint8_t /*bool*/ onsContextInit(t_ons_context *this_, t_ons_core *core_)
{
    this_->_core = core_;
    this_->_journal_times = this_->_journal = NULL;
    
    if(core_ == NULL) return 0;
    
    this_->_journal_times = minsonCreateArray(NULL);
    this_->_journal = minsonCreateArray(NULL);
    return 1;
}


void onsContextClose(t_ons_context *this_)
{
/* { */ _lock(&(this_->_core->__mutex));
    
    if(this_->_core->__sync_context == this_)
        this_->_core->__sync_context = NULL;
    
/* } */ _unlock(&(this_->_core->__mutex));
    
    this_->_core = NULL;
    
    minsonDeleteArray(this_->_journal_times); this_->_journal_times = NULL;
    minsonDeleteArray(this_->_journal); this_->_journal = NULL;
    return;
}


t_minson_object *onsExecute(t_ons_context *this_, t_minson_object *request_)
{
    t_minson_object *response = NULL;
    t_minson_array *rq;
    t_minson_iter it;
    
    if(request_->name == NULL) {}
    else if(strcmp(request_->name, "get_api") == 0)
    {
        response = minsonUpcast(rq = minsonCreateArray("ons"));
        minsonArrayAddNumber(rq, NULL, ONS_VERSION_MAJOR);
        minsonArrayAddNumber(rq, NULL, ONS_VERSION_MINOR);
    }
    else if(strcmp(request_->name, "get") == 0)
    {
        if((rq = minsonCastArray(request_)) == NULL) goto __end;
        if(! minsonNotEnd(it = minsonFirst(rq))) goto __end;
        
        char const *category = minsonIterName(it);
        char const *entry = minsonIterGetString(it);
        response = _get_entry_impl(this_, category, entry);
    }
    else if(strcmp(request_->name, "set") == 0)
    {
        if((rq = minsonCastArray(request_)) == NULL) goto __bool_end;
        if(! minsonNotEnd(it = minsonFirst(rq))) goto __bool_end;
        
        char const *category = minsonIterName(it);
        if((rq = minsonIterGetArray(it)) == NULL) goto __bool_end;
        if(! minsonNotEnd(it = minsonFirst(rq))) goto __bool_end;
        
        t_minson_object *entry = minsonIterObject(it);
        response = minsonCreateBoolean(
            NULL, _set_entry_impl(this_, category, entry)
        );
    }
    else if(strcmp(request_->name, "get_category") == 0)
    {
        char const *category = minsonGetString(request_);
        response = minsonUpcast(_get_category_impl(this_, category));
    }
    else if(strcmp(request_->name, "get_category_all") == 0)
    {
        char const *category = minsonGetString(request_);
        response = minsonUpcast(_get_category_all_impl(this_, category));
    }
    else if(strcmp(request_->name, "generate") == 0)
    {
        char const *category = minsonGetString(request_);
        char const *name = _generate_entry_impl(this_, category);
        if(name != NULL) response = minsonCreateString(NULL, name);
    }
    else if(strcmp(request_->name, "delete") == 0)
    {
        if((rq = minsonCastArray(request_)) == NULL) goto __bool_end;
        if(! minsonNotEnd(it = minsonFirst(rq))) goto __bool_end;
        
        char const *category = minsonIterName(it);
        char const *entry = minsonIterGetString(it);
        response = minsonCreateBoolean(
            NULL, _delete_entry_impl(this_, category, entry)
        );
    }
    else if(strcmp(request_->name, "flush") == 0)
    {
        response = minsonCreateBoolean(NULL, _flush_impl(this_));
    }
    else if(strcmp(request_->name, "reset") == 0)
    {
        response = minsonCreateBoolean(NULL, _reset_impl(this_));
    }
    else if(strcmp(request_->name, "set_time") == 0)
    {
        if((rq = minsonCastArray(request_)) == NULL) goto __bool_end;
        if(! minsonNotEnd(it = minsonFirst(rq))) goto __bool_end;
        
        char const *category = minsonIterName(it);
        if((rq = minsonIterGetArray(it)) == NULL) goto __bool_end;
        if(! minsonNotEnd(it = minsonFirst(rq))) goto __bool_end;
        
        char const *entry = minsonIterName(it);
        int64_t time__;
        if(! minsonIterGetNumber(it, &time__)) goto __bool_end;
        response = minsonCreateBoolean(
            NULL, _set_time_impl(this_, category, entry, time__)
        );
    }
    else if(strcmp(request_->name, "hard_delete") == 0)
    {
        if((rq = minsonCastArray(request_)) == NULL) goto __bool_end;
        if(! minsonNotEnd(it = minsonFirst(rq))) goto __bool_end;
        
        char const *category = minsonIterName(it);
        char const *entry = minsonIterGetString(it);
        if(! minsonNotEnd(minsonNext(it))) goto __bool_end;
        
        int64_t time__;
        if(! minsonIterGetNumber(it, &time__)) goto __bool_end;
        response = minsonCreateBoolean(
            NULL, _hard_delete_impl(this_, category, entry, time__)
        );
    }
    else if(strcmp(request_->name, "get_matrix") == 0)
    {
        response = minsonUpcast(_get_matrix_impl(this_));
    }
    else if(strcmp(request_->name, "set_matrix") == 0)
    {
        t_minson_array *matrix = minsonCastArray(request_);
        response = minsonCreateBoolean(NULL, _set_matrix_impl(this_, matrix));
    }
    else if(strcmp(request_->name, "get_table") == 0)
    {
        response = minsonUpcast(_get_table_impl(this_));
    }
    else if(strcmp(request_->name, "change_member_name") == 0)
    {
        char const *new_name = minsonGetString(request_);
        response = minsonCreateBoolean(NULL, _change_member_name(this_, new_name));
    }
    
__end:
    return (response != NULL)? response : minsonCreateNull(NULL);
__bool_end:
    return (response != NULL)? response : minsonCreateBoolean(NULL, 0);
}
