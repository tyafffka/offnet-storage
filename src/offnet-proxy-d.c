#include "m.h"
#include "offnet-storage.h"
#include <signal.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <stdio.h>

#ifndef _WIN32
# define N_SERVER_TYPES 2
#else
# define N_SERVER_TYPES 1
#endif


static char const *__server_types[N_SERVER_TYPES] = {
    "tcp",
#ifndef _WIN32
    "unix",
#endif
};


typedef struct t_proxy_client
{
    struct t_proxy_client *next;
    base_ons_client *client;
    base_ons_connection *sub_connection;
}
t_proxy_client;


typedef struct t_proxy_daemon
{
    t_minson_array *config;
    atomic_bool volatile is_run;
}
t_proxy_daemon;

// --- *** ---


static bool __serve_client(t_proxy_client *client, t_minson_array *gw_list_,
                           bool *out_is_idle_)
{
    t_minson_object *rq = NULL, *rs;
    
    if(! onsClientRequest(client->client, &rq)) return false;
    if(rq == NULL) return true;
    *out_is_idle_ = false;
    
    if(rq->name != NULL && strcmp(rq->name, "connect") == 0)
    {
        char const *gw_name = minsonGetString(rq);
        char const *url = minsonArrayGetString(gw_list_, gw_name);
        
        if(url != NULL)
        {
            onsDeleteConnection(client->sub_connection);
            client->sub_connection = onsCreateConnection(url, 0 /*no check api*/);
            rs = minsonCreateBoolean(NULL, (client->sub_connection != NULL)?
                                           (uint8_t)1 : (uint8_t)0);
        }
        else
        {   rs = minsonCreateBoolean(NULL, 0);   }
    }
    else if(client->sub_connection != NULL)
    {   rs = onsConnectionAdapter(client->sub_connection, rq);   }
    else
    {   rs = minsonCreateNull(NULL);   }
    
    minsonDelete(rq);
    bool r = onsClientResponse(client->client, rs);
    minsonDelete(rs);
    return r;
}


static void *ProxyThread(void *userdata_)
{
    t_proxy_daemon *Daemon = m_pcast(t_proxy_daemon *, userdata_);
    
    t_minson_array *gw_list = minsonArrayGetArray(Daemon->config, "gateways");
    if(gw_list == NULL)
    {
        fprintf(stderr, "Info: gateways list not specified. Proxy disabled.\n");
        return NULL;
    }
    if(minsonArrayNamedCount(gw_list) == 0)
    {
        fprintf(stderr, "Info: gateways list is empty. Proxy disabled.\n");
        return NULL;
    }
    
    base_ons_server *servers[N_SERVER_TYPES];
    bool is_serving = false;
    
    for(int i = 0; i < N_SERVER_TYPES; ++i)
    {
        servers[i] = onsCreateServer(__server_types[i], Daemon->config);
        if(servers[i] == NULL)
        {
            fprintf(stderr, "Info: server interface '%s' not created.\n",
                    __server_types[i]);
        }
        else
        {   is_serving = true;   }
    }
    
    if(! is_serving)
    {
        fprintf(stderr, "Info: no server interfaces created. Proxy disabled.\n");
        return NULL;
    }
    
    // ---
    
    t_proxy_client *clients_first = NULL;
    bool is_idle = false;
    
    while(Daemon->is_run)
    {
        if(is_idle)
        {
static struct timespec const __delay = (struct timespec const){0, 1};
            nanosleep(&__delay, NULL);
        }
        is_idle = true;
        
        // servers
        
        for(int i = 0; i < N_SERVER_TYPES; ++i)
        {
            if(servers[i] == NULL) continue;
            
            base_ons_client *client = onsNewClient(servers[i]);
            if(client == NULL) continue;
            
            is_idle = false;
            t_proxy_client *p = m_new(t_proxy_client, 1);
            
            fprintf(stderr, "Info: new client connected.\n");
            p->next = clients_first; clients_first = p;
            p->client = client;
            p->sub_connection = NULL;
        } //- foreach(server)
        
        // clients
        
        for(t_proxy_client *it = clients_first, *itpre = NULL, *itnext;
            it != NULL; itpre = it, it = itnext)
        {
            itnext = it->next;
            
            if(! __serve_client(it, gw_list, &is_idle))
            {
                fprintf(stderr, "Info: client disconnected.\n");
                onsDeleteConnection(it->sub_connection);
                onsDeleteClient(it->client);
                m_delete(it);
                
                it = itpre;
                if(itpre == NULL) clients_first = itnext;
                else itpre->next = itnext;
            }
        } //- foreach(client)
    } //- proxy loop
    
    // ---
    
    for(t_proxy_client *it = clients_first; it != NULL;)
    {
        t_proxy_client *itpre = it; it = it->next;
        
        onsDeleteConnection(it->sub_connection);
        onsDeleteClient(itpre->client);
        m_delete(itpre);
    }
    
    for(int i = 0; i < N_SERVER_TYPES; ++i)
        onsDeleteServer(servers[i]);
    return NULL;
}

// --- *** ---


static t_proxy_daemon mDaemon;


static void __terminate_handler(int signal_)
{
    fprintf(stderr, "\nInfo: received signal '%s'. Terminating.\n",
            strsignal(signal_));
    
    mDaemon.is_run = false;
    return;
}


int /*nix*/ main(int argc, char *const *argv)
{
    pthread_t proxy_thread;
    int r = 1;
    
    char const *config_file;
    if(argc < 2)
    {   config_file = ONS_PROXY_CONFIG_FILE;   }
    else
    {
        config_file = argv[1];
        if(config_file[0] == '\0')
        {
            fprintf(stderr, "Error: path to config file is empty!\n");
            return r;
        }
    }
    
    r = 2;
    {
        t_minson_object *config_obj = minsonParseFile(config_file);
        mDaemon.config = minsonCastArray(config_obj);
        
        if(mDaemon.config == NULL)
        {
            fprintf(stderr, "Error: proxy config '%s' corrupted!\n",
                    config_file);
            minsonDelete(config_obj);
            return r;
        }
    }
    
    mDaemon.is_run = ATOMIC_VAR_INIT(true);
    
    // Start serving
    
#if defined(_WIN32)
    signal(SIGABRT, __terminate_handler);
    signal(SIGINT,  __terminate_handler);
    signal(SIGTERM, __terminate_handler);
    signal(SIGQUIT, __terminate_handler);
#elif defined(__unix__)
    static struct sigaction h_action;
    h_action.sa_handler = __terminate_handler;
    
    sigemptyset(&h_action.sa_mask);
    sigaddset(&h_action.sa_mask, SIGABRT);
    sigaddset(&h_action.sa_mask, SIGINT);
    sigaddset(&h_action.sa_mask, SIGTERM);
    sigaddset(&h_action.sa_mask, SIGQUIT);
    
    h_action.sa_flags = 0;
    
    sigaction(SIGABRT, &h_action, NULL);
    sigaction(SIGINT,  &h_action, NULL);
    sigaction(SIGTERM, &h_action, NULL);
    sigaction(SIGQUIT, &h_action, NULL);
#endif
    
    r = 5;
    if(pthread_create(&proxy_thread, NULL, ProxyThread, (void *)&mDaemon) != 0)
    {
        fprintf(stderr, "Error: proxy thread not started!\n");
        goto __end1;
    }
    
    r = 0;
    fprintf(stderr, "Info: offnet proxy served...\n");
    
    pthread_join(proxy_thread, NULL);
__end1:
    minsonDeleteArray(mDaemon.config);
    return r;
}
