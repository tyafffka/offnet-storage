#include "interface.h"
#include "_sync_interface.h"


t_minson_array *onsGetCategory(base_ons_connection *this_,
                               char const *category_)
{
    if(category_ == NULL) return NULL;
    
    t_minson_object *rq = minsonCreateString("get_category", category_);
    t_minson_array *rs = minsonCastArray(onsConnectionAdapter(this_, rq));
    minsonDelete(rq);
    return rs;
}


t_minson_array *onsGetCategoryAll(base_ons_connection *this_,
                                  char const *category_)
{
    if(category_ == NULL) return NULL;
    
    t_minson_object *rq = minsonCreateString("get_category_all", category_);
    t_minson_array *rs = minsonCastArray(onsConnectionAdapter(this_, rq));
    minsonDelete(rq);
    return rs;
}


char const *onsGenerateEntry(base_ons_connection *this_,
                             char const *category_)
{
    if(category_ == NULL) return NULL;
    
    t_minson_object *rq = minsonCreateString("generate", category_);
    t_minson_object *rs = onsConnectionAdapter(this_, rq);
    minsonDelete(rq);
    return minsonGetString(rs);
}


t_minson_object *onsGetEntry(base_ons_connection *this_,
                             char const *category_, char const *entry_)
{
    if(category_ == NULL || entry_ == NULL) return NULL;
    
    t_minson_array *rq = minsonCreateArray("get");
    minsonArrayAddString(rq, category_, entry_);
    t_minson_object *rs = onsConnectionAdapter(this_, minsonUpcast(rq));
    minsonDeleteArray(rq);
    
    if(rs != NULL) if(rs->name == NULL) return NULL;
    return rs;
}


uint8_t /*bool*/ onsSetEntry(base_ons_connection *this_,
                             char const *category_, t_minson_object *entry_)
{
    if(category_ == NULL || entry_ == NULL) return 0;
    if(entry_->name == NULL) return 0;
    
    t_minson_array *rq = minsonCreateArray("set");
    t_minson_array *cat = minsonCreateArray(category_);
    minsonArrayAdd(rq, minsonUpcast(cat));
    minsonArrayAdd(cat, entry_);
    t_minson_object *rs = onsConnectionAdapter(this_, minsonUpcast(rq));
    
    minsonArrayRemove(cat, entry_->name);
    minsonDeleteArray(rq);
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    return r;
}


uint8_t /*bool*/ onsDeleteEntry(base_ons_connection *this_,
                                char const *category_, char const *entry_)
{
    if(category_ == NULL || entry_ == NULL) return 0;
    
    t_minson_array *rq = minsonCreateArray("delete");
    minsonArrayAddString(rq, category_, entry_);
    t_minson_object *rs = onsConnectionAdapter(this_, minsonUpcast(rq));
    minsonDeleteArray(rq);
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    return r;
}


uint8_t /*bool*/ onsFlush(base_ons_connection *this_)
{
    t_minson_object *rq = minsonCreateNull("flush");
    t_minson_object *rs = onsConnectionAdapter(this_, rq);
    minsonDelete(rq);
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    return r;
}


uint8_t /*bool*/ onsReset(base_ons_connection *this_)
{
    t_minson_object *rq = minsonCreateNull("reset");
    t_minson_object *rs = onsConnectionAdapter(this_, rq);
    minsonDelete(rq);
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    return r;
}


uint8_t /*bool*/ onsChangeMemberName(base_ons_connection *this_,
                                     char const *new_name_)
{
    if(new_name_ == NULL) return 0;
    
    t_minson_object *rq = minsonCreateString("change_member_name", new_name_);
    t_minson_object *rs = onsConnectionAdapter(this_, rq);
    minsonDelete(rq);
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    return r;
}

// --- *** ---


t_minson_array *_get_matrix(base_ons_connection *this_)
{
    t_minson_object *rq = minsonCreateNull("get_matrix");
    t_minson_object *rs = onsConnectionAdapter(this_, rq);
    minsonDelete(rq);
    return minsonCastArray(rs);
}


uint8_t /*bool*/ _set_matrix(base_ons_connection *this_,
                             t_minson_array *matrix_)
{
    if(matrix_ == NULL) return 0;
    
    char const *old_name = matrix_->h.name;
    matrix_->h.name = "set_matrix";
    t_minson_object *rs = onsConnectionAdapter(this_, minsonUpcast(matrix_));
    matrix_->h.name = old_name;
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    return r;
}


t_minson_object *_get_table(base_ons_connection *this_)
{
    t_minson_object *rq = minsonCreateNull("get_table");
    t_minson_object *rs = onsConnectionAdapter(this_, rq);
    minsonDelete(rq);
    
    if(rs->type != C_MINSON_ARRAY) return NULL;
    return rs;
}


uint8_t /*bool*/ _set_time(base_ons_connection *this_,
                           char const *category_, char const *entry_,
                           int64_t time_)
{
    if(category_ == NULL || entry_ == NULL || time_ <= 0) return 0;
    
    t_minson_array *rq = minsonCreateArray("set_time");
    t_minson_array *cat = minsonCreateArray(category_);
    minsonArrayAdd(rq, minsonUpcast(cat));
    minsonArrayAddNumber(cat, entry_, time_);
    t_minson_object *rs = onsConnectionAdapter(this_, minsonUpcast(rq));
    
    minsonDeleteArray(rq);
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    return r;
}


uint8_t /*bool*/ _hard_delete(base_ons_connection *this_,
                              char const *category_, char const *entry_,
                              int64_t horizon_)
{
    if(category_ == NULL || entry_ == NULL) return 0;
    
    t_minson_array *rq = minsonCreateArray("hard_delete");
    minsonArrayAddString(rq, category_, entry_);
    minsonArrayAddNumber(rq, NULL, horizon_);
    t_minson_object *rs = onsConnectionAdapter(this_, minsonUpcast(rq));
    minsonDeleteArray(rq);
    
    uint8_t r;
    if(! minsonGetBoolean(rs, &r)) r = 0;
    return r;
}
