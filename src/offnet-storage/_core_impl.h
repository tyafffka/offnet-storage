#ifndef __OFFNET_STORAGE_CORE_IMPL_H
#define __OFFNET_STORAGE_CORE_IMPL_H

#include "core.h"


t_minson_array *_get_category_impl(t_ons_context *this_,
                                   char const *category_);
t_minson_array *_get_category_all_impl(t_ons_context *this_,
                                       char const *category_);

char const *_generate_entry_impl(t_ons_context *this_,
                                 char const *category_);

t_minson_object *_get_entry_impl(t_ons_context *this_,
                                 char const *category_, char const *entry_);
uint8_t /*bool*/ _set_entry_impl(t_ons_context *this_,
                                 char const *category_, t_minson_object *entry_);
uint8_t /*bool*/ _delete_entry_impl(t_ons_context *this_,
                                    char const *category_, char const *entry_);

uint8_t /*bool*/ _flush_impl(t_ons_context *this_);
uint8_t /*bool*/ _reset_impl(t_ons_context *this_);

// ---

char *__get_member_name(t_ons_context *this_);

// ---

uint8_t /*bool*/ _change_member_name(t_ons_context *this_,
                                     char const *new_name_);

t_minson_array *_get_matrix_impl(t_ons_context *this_);
uint8_t /*bool*/ _set_matrix_impl(t_ons_context *this_,
                                  t_minson_array *matrix_);

t_minson_array *_get_table_impl(t_ons_context *this_);

uint8_t /*bool*/ _set_time_impl(t_ons_context *this_,
                                char const *category_, char const *entry_,
                                int64_t time_);
uint8_t /*bool*/ _hard_delete_impl(t_ons_context *this_,
                                   char const *category_, char const *entry_,
                                   int64_t horizon_);

#endif // __OFFNET_STORAGE_CORE_IMPL_H
