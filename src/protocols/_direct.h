#ifndef __OFFNET_STORAGE_DIRECT_PROTOCOL_H
#define __OFFNET_STORAGE_DIRECT_PROTOCOL_H

#include "offnet-storage/protocols.h"
#include "offnet-storage/core.h"


typedef struct _base_ons_direct_connection
{
    base_ons_connection h;
    t_ons_context __context;
}
_base_ons_direct_connection;


typedef struct _t_ons_direct_connection
{
    _base_ons_direct_connection h;
    t_ons_core __core;
}
_t_ons_direct_connection;


base_ons_connection *_onsCreateBaseDirect(t_ons_core *core_);
base_ons_connection *_onsCreateDirect(char const *params_);


#endif // __OFFNET_STORAGE_DIRECT_PROTOCOL_H
