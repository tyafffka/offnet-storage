#ifndef _OFFNET_STORAGE_INTERFACE_H
#define _OFFNET_STORAGE_INTERFACE_H

#include "protocols.h"

#ifdef __cplusplus
extern "C" {
#endif


/* _Внимание!_ Любые указатели, возвращённые следующей группой функций,
 * действительны только до следующего вызова функции
 * из этой группы или до закрытия соединения! */

/* Возвращает массив записей запрошенной категории
 * (время последнего изменения в качестве значения). */
t_minson_array *onsGetCategory(base_ons_connection *this_,
                               char const *category_);
/* Возвращает массив всех записей категории, включая удалённые записи.
 * Для удалённых время последнего изменения указывается со знаком минус. */
t_minson_array *onsGetCategoryAll(base_ons_connection *this_,
                                  char const *category_);

/* Создать новую запись в указанной категории
 * с произвольным именем. Возвращает имя записи. */
char const *onsGenerateEntry(base_ons_connection *this_,
                             char const *category_);

/* Возвращает MinSON-объект, сохранённый в указанной записи,
 * или `NULL` в случае ошибки. */
t_minson_object *onsGetEntry(base_ons_connection *this_,
                             char const *category_, char const *entry_);
/* Сохранить MinSON-объект как запись в указанной категории.
 * Объект должен быть именованным. */
uint8_t /*bool*/ onsSetEntry(base_ons_connection *this_,
                             char const *category_, t_minson_object *entry_);
/* Удалить указанную запись. */
uint8_t /*bool*/ onsDeleteEntry(base_ons_connection *this_,
                                char const *category_, char const *entry_);

/* Зафиксировать (синхронизировать) изменения в хранилище. */
uint8_t /*bool*/ onsFlush(base_ons_connection *this_);
/* Отменить последние изменения и восстановить актуальное состояние хранилища. */
uint8_t /*bool*/ onsReset(base_ons_connection *this_);

/* Изменяет имя абонента хранилища и правит матрицу актуальности
 * соответствующим образом. */
uint8_t /*bool*/ onsChangeMemberName(base_ons_connection *this_,
                                     char const *new_name_);
// ---

/* Полная процедура синхронизации двух хранилищ
 * (информация об алгоритме [здесь](doc/synchronization.md)). */
uint8_t /*bool*/ onsSynchronization(base_ons_connection *storage1_,
                                    base_ons_connection *storage2_);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _OFFNET_STORAGE_INTERFACE_H
