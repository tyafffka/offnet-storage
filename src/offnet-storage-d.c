#include "m.h"
#include "offnet-storage.h"
#include <signal.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <time.h>
#include <stdio.h>

#ifndef _WIN32
# define N_SERVER_TYPES 2
#else
# define N_SERVER_TYPES 1
#endif


static char const *__server_types[N_SERVER_TYPES] = {
    "tcp",
#ifndef _WIN32
    "unix",
#endif
};


typedef struct t_client
{
    struct t_client *next;
    base_ons_client *client;
    t_ons_context context;
}
t_client;


typedef struct t_daemon
{
    t_minson_array *config;
    t_ons_core core;
    atomic_bool volatile is_run;
}
t_daemon;

// --- *** ---


static void *ServeThread(void *userdata_)
{
    t_daemon *Daemon = m_pcast(t_daemon *, userdata_);
    
    base_ons_server *servers[N_SERVER_TYPES];
    bool is_serving = false;
    
    for(int i = 0; i < N_SERVER_TYPES; ++i)
    {
        servers[i] = onsCreateServer(__server_types[i], Daemon->config);
        if(servers[i] == NULL)
        {
            fprintf(stderr, "Info: server interface '%s' not created.\n",
                    __server_types[i]);
        }
        else
        {   is_serving = true;   }
    }
    
    if(! is_serving)
    {
        fprintf(stderr, "Info: no server interfaces created. Serving disabled.\n");
        return NULL;
    }
    
    // ---
    
    t_client *clients_first = NULL;
    bool is_idle = false;
    
    while(Daemon->is_run)
    {
        if(is_idle)
        {
static struct timespec const __delay = (struct timespec const){0, 1};
            nanosleep(&__delay, NULL);
        }
        is_idle = true;
        
        // servers
        
        for(int i = 0; i < N_SERVER_TYPES; ++i)
        {
            if(servers[i] == NULL) continue;
            
            base_ons_client *client = onsNewClient(servers[i]);
            if(client == NULL) continue;
            
            is_idle = false;
            t_client *p = m_new(t_client, 1);
            
            if(onsContextInit(&(p->context), &(Daemon->core)))
            {
                fprintf(stderr, "Info: new client connected.\n");
                p->next = clients_first; clients_first = p;
                p->client = client;
            }
            else
            {
                fprintf(stderr, "Error: new context init failed!\n");
                onsDeleteClient(client);
                m_delete(p);
            }
        } //- foreach(server)
        
        // clients
        
        for(t_client *it = clients_first, *itpre = NULL, *itnext;
            it != NULL; itpre = it, it = itnext)
        {
            itnext = it->next;
            
            t_minson_object *rq = NULL;
            bool is_disconnected = false;
            
            if(! onsClientRequest(it->client, &rq))
            {   is_disconnected = true;   }
            else if(rq != NULL)
            {
                is_idle = false;
                
                t_minson_object *rs = onsExecute(&(it->context), rq);
                minsonDelete(rq);
                
                if(! onsClientResponse(it->client, rs))
                {   is_disconnected = true;   }
                minsonDelete(rs);
            }
            
            if(is_disconnected) // close connection
            {
                fprintf(stderr, "Info: client disconnected.\n");
                onsContextClose(&(it->context));
                onsDeleteClient(it->client);
                m_delete(it);
                
                it = itpre;
                if(itpre == NULL) clients_first = itnext;
                else itpre->next = itnext;
            }
        } //- foreach(client)
    } //- serve loop
    
    // ---
    
    for(t_client *it = clients_first; it != NULL;)
    {
        t_client *itpre = it; it = it->next;
        
        onsContextClose(&(itpre->context));
        onsDeleteClient(itpre->client);
        m_delete(itpre);
    }
    
    for(int i = 0; i < N_SERVER_TYPES; ++i)
        onsDeleteServer(servers[i]);
    return NULL;
}

// --- *** ---


static void *SyncThread(void *userdata_)
{
    t_daemon *Daemon = m_pcast(t_daemon *, userdata_);
    
    int64_t sync_period;
    if(! minsonArrayGetNumber(Daemon->config, "sync-period", &sync_period))
    {
        fprintf(stderr, "Info: sync period not specified. Syncronization disabled.\n");
        return NULL;
    }
    
    t_minson_array *sync_list = minsonArrayGetArray(Daemon->config, "sync-list");
    if(sync_list == NULL)
    {
        fprintf(stderr, "Info: sync list not specified. Syncronization disabled.\n");
        return NULL;
    }
    if(minsonArrayCount(sync_list) == 0)
    {
        fprintf(stderr, "Info: sync list is empty. Syncronization disabled.\n");
        return NULL;
    }
    
    // ---
    
    time_t next_sync = time(NULL) + sync_period;
    
    while(Daemon->is_run)
    {
static struct timespec const __delay = (struct timespec const){0, 100000000 /*0.1s*/};
        nanosleep(&__delay, NULL);
        
        time_t curr_time = time(NULL);
        if(curr_time < next_sync) continue;
        next_sync += sync_period;
        
        base_ons_connection *self = _onsCreateBaseDirect(&(Daemon->core));
        if(self == NULL) continue;
        
        for(t_minson_iter it = minsonFirst(sync_list); minsonNotEnd(it); minsonNext(it))
        {
            char const *remote_url = minsonIterGetString(it);
            if(remote_url == NULL) continue;
            
            base_ons_connection *remote = onsCreateConnection(remote_url, 1 /*check api*/);
            if(remote == NULL)
            {
                fprintf(stderr, "Error: can't connect to remote storage '%s'!\n",
                        remote_url);
                continue;
            }
            
            if(! onsSynchronization(self, remote))
            {
                fprintf(stderr, "Error: internal error during synchronization with '%s'!\n",
                        remote_url);
            }
            onsDeleteConnection(remote);
        }
        
        onsDeleteConnection(self);
    } //- sync loop
    return NULL;
}

// --- *** ---


static t_daemon mDaemon;


static void __terminate_handler(int signal_)
{
    fprintf(stderr, "\nInfo: received signal '%s'. Terminating.\n",
            strsignal(signal_));
    
    mDaemon.is_run = false;
    return;
}


int /*nix*/ main(int argc, char *const *argv)
{
    pthread_t serve_thread, sync_thread;
    int r = 0;
    
    if(argc < 2)
    {
        fprintf(stderr,
"Usage: %s <storage-dir>\n"
"  <storage-dir> - path to storage that been served.\n",
            argv[0]
        );
        return r;
    }
    r = 1;
    
    char const *storage_dir = argv[1];
    if(storage_dir[0] == '\0')
    {
        fprintf(stderr, "Error: path to storage is empty!\n");
        return r;
    }
    
    r = 2;
    {
        size_t dir_len = strlen(storage_dir);
        int need_slash = (storage_dir[dir_len - 1] != '/' &&
                          storage_dir[dir_len - 1] != '\\')? 1 : 0;
        
        char config_path[dir_len + need_slash + sizeof(ONS_CONFIG_FILE) /*- 1 + 1*/];
        strcpy(config_path, storage_dir);
        if(need_slash) config_path[dir_len] = '/';
        strcpy(config_path + dir_len + need_slash, ONS_CONFIG_FILE);
        
        t_minson_object *config_obj = minsonParseFile(config_path);
        mDaemon.config = minsonCastArray(config_obj);
        
        if(mDaemon.config == NULL)
        {
            fprintf(stderr, "Error: storage config '%s' corrupted!\n",
                    config_path);
            minsonDelete(config_obj);
            return r;
        }
    }
    
    r = 3;
    if(! onsCoreInit(&(mDaemon.core), storage_dir))
    {
        fprintf(stderr, "Error: storage '%s' not inited!\n",
                storage_dir);
        goto __end1;
    }
    
    mDaemon.is_run = ATOMIC_VAR_INIT(true);
    
    // Start serving
    
#if defined(_WIN32)
    signal(SIGABRT, __terminate_handler);
    signal(SIGINT,  __terminate_handler);
    signal(SIGTERM, __terminate_handler);
    signal(SIGQUIT, __terminate_handler);
#elif defined(__unix__)
static struct sigaction h_action;
    h_action.sa_handler = __terminate_handler;
    
    sigemptyset(&h_action.sa_mask);
    sigaddset(&h_action.sa_mask, SIGABRT);
    sigaddset(&h_action.sa_mask, SIGINT);
    sigaddset(&h_action.sa_mask, SIGTERM);
    sigaddset(&h_action.sa_mask, SIGQUIT);
    
    h_action.sa_flags = 0;
    
    sigaction(SIGABRT, &h_action, NULL);
    sigaction(SIGINT,  &h_action, NULL);
    sigaction(SIGTERM, &h_action, NULL);
    sigaction(SIGQUIT, &h_action, NULL);
#endif
    
    r = 4;
    if(pthread_create(&sync_thread, NULL, SyncThread, (void *)&mDaemon) != 0)
    {
        fprintf(stderr, "Error: sync thread not started!\n");
        goto __end2;
    }
    
    r = 5;
    if(pthread_create(&serve_thread, NULL, ServeThread, (void *)&mDaemon) != 0)
    {
        fprintf(stderr, "Error: server thread not started!\n");
        goto __end3;
    }
    
    r = 0;
    fprintf(stderr, "Info: storage '%s' served...\n",
            storage_dir);
    
    pthread_join(serve_thread, NULL);
__end3:
    mDaemon.is_run = false;
    pthread_join(sync_thread, NULL);
__end2:
    onsCoreClose(&(mDaemon.core));
__end1:
    minsonDeleteArray(mDaemon.config);
    return r;
}
