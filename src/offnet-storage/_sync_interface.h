#ifndef __OFFNET_STORAGE_SYNC_INTERFACE_H
#define __OFFNET_STORAGE_SYNC_INTERFACE_H

#include "protocols.h"


/* Возвращает матрицу актуальности, где элемент с именем самого абонента
 * хранилища содержит список актуальности, а остальные элементы --
 * несобственные горизонты актуальности. */
t_minson_array *_get_matrix(base_ons_connection *this_);
/* Присваивает хранилищу матрицу актуальности (формат смотри выше). */
uint8_t /*bool*/ _set_matrix(base_ons_connection *this_,
                             t_minson_array *matrix_);

/* Возвращает полный список всех записей всех категорий,
 * включая отмеченные как удалённые: для них время последнего
 * изменения указывается со знаком минус. */
t_minson_object *_get_table(base_ons_connection *this_);

/* Устанавливает время последнего изменения для записи. */
uint8_t /*bool*/ _set_time(base_ons_connection *this_,
                           char const *category_, char const *entry_,
                           int64_t time_);
/* Физически удаляет запись из хранилища. */
uint8_t /*bool*/ _hard_delete(base_ons_connection *this_,
                              char const *category_, char const *entry_,
                              int64_t horizon_);

#endif // __OFFNET_STORAGE_SYNC_INTERFACE_H
