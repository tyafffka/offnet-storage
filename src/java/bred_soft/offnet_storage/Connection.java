package bred_soft.offnet_storage;

import bred_soft.minson.*;



public final class Connection
{
    static
    {
        System.loadLibrary("offnet-storage-jn");
    }
    
    private long __p;
    
    private native void __init(String url_);
    private native void __destruct();
    
    // ---
    
    protected void finalize() throws Throwable
    {
        __destruct(); super.finalize();
    }
    
    // ---
    
    public static class URLError extends Error
    {
        URLError() { super(); }
        URLError(String message) { super(message); }
    }
    
    public static class NotFoundException extends Error
    {
        NotFoundException() { super(); }
        NotFoundException(String message) { super(message); }
    }
    
    
    public Connection(String url_)
    {
        __p = 0;
        if(url_ == null) throw new NullPointerException();
        __init(url_);
    }
    
    public void destroy()
    {
        __destruct(); __p = 0;
    }
    
    public native MinSONArray getCategory(String category_);
    public native MinSONArray getCategoryAll(String category_);
    public native String generate(String category_);
    
    public native MinSONObject get(String category_, String entry_);
    public native boolean set(String category_, MinSONObject entry_);
    public native boolean remove(String category_, String entry_);
    
    public native boolean flush();
    public native boolean reset();
    
    public native boolean changeMemberName(String new_name_);
    
    public static native boolean synchronization(Connection c1_, Connection c2_);
}
