#include "offnet-storage.hpp"
#include "test.h"


int main(int argc, char *const *argv)
{
    _TEST_(argc == 3);
    try
    {
        ons::connection Storage = ons::connection(argv[1]);
        _TEST_(Storage.changeMemberName(argv[2]));
    }
    catch(ons::url_error &)
    {
        _FAIL_("wrong URL's!");
    }
    _ENDTEST_;
}
