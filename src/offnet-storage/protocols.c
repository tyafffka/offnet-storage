#include "m.h"
#include "protocols.h"
#include "offnet-storage/config.h"


typedef struct __t_protocol
{
    t_minson_object h;
    t_ons_connection_constructor connection_constructor;
    t_ons_server_constructor server_constructor;
}
__t_protocol;

static t_minson_array *__protocols = NULL;

// --- *** ---


static uint8_t /*bool*/ __check_api(base_ons_connection *this_)
{
    t_minson_object *rq = minsonCreateNull("get_api");
    t_minson_array *rs = minsonCastArray(onsConnectionAdapter(this_, rq));
    minsonDelete(rq);
    if(rs == NULL) return 0;
    
    t_minson_iter it = minsonFirst(rs);
    int64_t major, minor;
    if(! minsonIterGetNumber(it, &major)) return 0;
    if(! minsonNotEnd(minsonNext(it))) return 0;
    if(! minsonIterGetNumber(it, &minor)) return 0;
    
    return (major == ONS_VERSION_MAJOR && minor >= ONS_VERSION_MINOR)?
           (uint8_t)1 : (uint8_t)0;
}


base_ons_connection *onsCreateConnection(char const *url_,
                                         uint8_t /*bool*/ check_api_)
{
    char const *p_end = strchr(url_, ':');
    if(p_end == NULL || p_end == url_) return NULL;
    
    char protocol[p_end - url_ + 1];
    memcpy(protocol, url_, p_end - url_);
    protocol[p_end - url_] = '\0';
    
    __t_protocol *proto =
        m_pcast(__t_protocol *, minsonArrayGet(__protocols, protocol));
    if(proto == NULL) return NULL;
    
    base_ons_connection *this = (proto->connection_constructor)(p_end + 1);
    if(this == NULL) return NULL;
    
    this->__last_response = NULL;
    if(check_api_) if(! __check_api(this))
    {
        onsDeleteConnection(this);
        return NULL;
    }
    return this;
}


void onsDeleteConnection(base_ons_connection *this_)
{
    if(this_ == NULL) return;
    
    minsonDelete(this_->__last_response);
    this_->__last_response = NULL;
    
    if(this_->v_destructor != NULL)
        (this_->v_destructor)(this_);
    else
        m_delete(this_);
    return;
}


t_minson_object *onsConnectionAdapter(base_ons_connection *this_,
                                      t_minson_object *request_)
{
    t_minson_object *response = (this_->v_adapter)(this_, request_);
    if(response != NULL)
    {
        minsonDelete(this_->__last_response);
        this_->__last_response = response;
    }
    return response;
}

// --- *** ---


base_ons_server *onsCreateServer(char const *protocol_,
                                 t_minson_array *params_)
{
    if(protocol_ == NULL) return NULL;
    if(protocol_[0] == '\0') return NULL;
    
    __t_protocol *proto =
        m_pcast(__t_protocol *, minsonArrayGet(__protocols, protocol_));
    if(proto == NULL) return NULL;
    
    return (proto->server_constructor)(params_);
}


void onsDeleteServer(base_ons_server *this_)
{
    if(this_ == NULL) return;
    
    if(this_->v_destructor != NULL)
        (this_->v_destructor)(this_);
    else
        m_delete(this_);
    return;
}

// --- *** ---


void onsDeleteClient(base_ons_client *this_)
{
    if(this_ == NULL) return;
    
    if(this_->v_destructor != NULL)
        (this_->v_destructor)(this_);
    else
        m_delete(this_);
    return;
}

// --- *** ---


void onsRegisterProtocol(char const *protocol_,
                         t_ons_connection_constructor connection_constructor_,
                         t_ons_server_constructor server_constructor_)
{
    if(protocol_ == NULL) return;
    if(protocol_[0] == '\0') return;
    
    if(__protocols == NULL)
    {
        __protocols = m_new(t_minson_array, 1);
        __protocols->h.name = NULL;
        __protocols->h.type = C_MINSON_ARRAY;
        minsonArrayInit(__protocols);
    }
    
    if(minsonArrayGet(__protocols, protocol_) != NULL) return;
    
    __t_protocol *proto = m_new(__t_protocol, 1);
    proto->h.name = m_strdup(protocol_);
    proto->h.type = C_MINSON_NULL;
    proto->connection_constructor = connection_constructor_;
    proto->server_constructor = server_constructor_;
    
    minsonArrayAdd(__protocols, m_pcast(t_minson_object *, proto));
    return;
}


void _onsClearProtocols()
{
    if(__protocols == NULL) return;
    
    for(t_minson_iter it = minsonFirst(__protocols);
        minsonNotEnd(it); minsonNext(it))
    {
        __t_protocol *proto = m_pcast(__t_protocol *, minsonIterObject(it));
        m_delete(proto->h.name);
        m_delete(proto);
    }
    
    minsonArrayClear(__protocols);
    m_delete(__protocols); __protocols = NULL;
    return;
}
