#include "offnet-storage.h"
#include <string.h>
#include <stdio.h>


static int /*nix*/ SyncStorage(char const *dir_)
{
    t_minson_object *config_obj = NULL;
    t_minson_array *config = NULL;
    base_ons_connection *storage = NULL;
    size_t dir_len = strlen(dir_);
    int r = 1;
    
    // Load config
    
    {
        char path[dir_len + 1 /*slash*/ + sizeof(ONS_CONFIG_FILE) /*- 1 + 1*/];
        sprintf(path, "%s/%s", dir_, ONS_CONFIG_FILE);
        config = minsonCastArray(config_obj = minsonParseFile(path));
    }
    if(config == NULL)
    {
        fprintf(stderr, "Error: can't read config file '%s/%s'!\n",
                dir_, ONS_CONFIG_FILE);
        goto __end;
    }
    r = 2;
    
    t_minson_array *sync_list = minsonArrayGetArray(config, "sync-list");
    if(sync_list == NULL)
    {
        fprintf(stderr, "Error: can't find sync list in config '%s/%s'!\n",
                dir_, ONS_CONFIG_FILE);
        goto __end;
    }
    if(minsonArrayCount(sync_list) == 0)
    {
        fprintf(stderr, "Info: it has no one to synchronize storage '%s' with.\n",
                dir_);
        r = 0; goto __end;
    }
    r = 3;
    
    // Try connect to storage
    
    {
        char url[sizeof("direct:") /*- 1*/ + dir_len /*+ 1*/];
        sprintf(url, "direct:%s", dir_);
        storage = onsCreateConnection(url, 0 /*no check api*/);
    }
    if(storage == NULL)
    {
        char const *socket_file = minsonArrayGetString(config, "socket-file");
        if(socket_file != NULL)
        {
            char url[sizeof("unix:") /*- 1*/ + strlen(socket_file) /*+ 1*/];
            sprintf(url, "unix:%s", socket_file);
            storage = onsCreateConnection(url, 1 /*check api*/);
        }
    }
    if(storage == NULL)
    {
        char const *ip = minsonArrayGetString(config, "ip");
        int64_t port;
        if(ip != NULL && minsonArrayGetNumber(config, "port", &port))
        {
            char url[sizeof("tcp:") /*- 1*/ + strlen(ip) + 6 /*colon + digits*/ /*+ 1*/];
            sprintf(url, "tcp:%s:%d", ip, (int)port);
            storage = onsCreateConnection(url, 1 /*check api*/);
        }
    }
    if(storage == NULL)
    {
        fprintf(stderr, "Error: can't connect to storage '%s'!\n",
                dir_);
        goto __end;
    }
    r = 0;
    
    // Sync by list
    
    uint32_t synched = 0;
    for(t_minson_iter it = minsonFirst(sync_list); minsonNotEnd(it); minsonNext(it))
    {
        char const *remote_url = minsonIterGetString(it);
        if(remote_url == NULL) continue;
        
        base_ons_connection *remote = onsCreateConnection(remote_url, 1 /*check api*/);
        if(remote == NULL)
        {
            fprintf(stderr, "Error: can't connect to remote storage '%s'!\n",
                    remote_url);
            continue;
        }
        
        if(! onsSynchronization(storage, remote))
        {
            fprintf(stderr, "Error: internal error during synchronization with '%s'!\n",
                    remote_url);
            r = 4;
        }
        else
        {   ++synched;   }
        
        onsDeleteConnection(remote);
    }
    
    fprintf(stderr, "Info: storage '%s' synchronized with %d of %d remote storages.\n",
            dir_, synched, minsonArrayCount(sync_list));
__end:
    minsonDelete(config_obj);
    onsDeleteConnection(storage);
    return r;
}

// --- *** ---


int /*nix*/ main(int argc, char *const *argv)
{
    if(argc < 2)
    {
        fprintf(stderr,
"Usage: %s <storage-dir>\n"
"  <storage-dir> - path to storage that been synchronized by sync-list\n",
            argv[0]
        );
        return 0;
    }
    else
    {   return SyncStorage(argv[1]);   }
}
