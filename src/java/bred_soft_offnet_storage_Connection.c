#include "bred_soft_offnet_storage_Connection.h"
#include <offnet-storage.h>
#include <stdbool.h>

extern JNIIMPORT jobject JNICALL
    MinSON_instance(JNIEnv *env_, t_minson_object *obj_, bool is_own_);
extern JNIIMPORT t_minson_object *JNICALL
    MinSON_obj(JNIEnv *env_, jobject this_);



JNIEXPORT base_ons_connection *JNICALL ONS_connection(JNIEnv *env_, jobject this_)
{
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID p_f__ = (*env_)->GetFieldID(env_, class__, "__p", "J");
    
    return (base_ons_connection *)(intptr_t)((*env_)->GetLongField(env_, this_, p_f__));
}

// --- *** ---


JNIEXPORT void JNICALL Java_bred_1soft_offnet_1storage_Connection__1_1init
    (JNIEnv *env_, jobject this_, jstring url_)
{
    char const *url = (*env_)->GetStringUTFChars(env_, url_, NULL);
    
    base_ons_connection *__p = onsCreateConnection(url, 1 /*true*/);
    
    (*env_)->ReleaseStringUTFChars(env_, url_, url);
    
    if(__p == NULL)
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/offnet_storage/Connection$URLError");
        (*env_)->ThrowNew(env_, exception, "Can't initialize connection by url!");
        return;
    }
    
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID p_f__ = (*env_)->GetFieldID(env_, class__, "__p", "J");
    (*env_)->SetLongField(env_, this_, p_f__, (jlong)(intptr_t)__p);
    return;
}

JNIEXPORT void JNICALL Java_bred_1soft_offnet_1storage_Connection__1_1destruct
    (JNIEnv *env_, jobject this_)
{
    base_ons_connection *__p = ONS_connection(env_, this_);
    
    if(__p != NULL) onsDeleteConnection(__p);
    return;
}

// ---

JNIEXPORT jobject JNICALL Java_bred_1soft_offnet_1storage_Connection_getCategory
    (JNIEnv *env_, jobject this_, jstring category_)
{
    char const *category = (*env_)->GetStringUTFChars(env_, category_, NULL);
    if(category == NULL) return NULL;
    base_ons_connection *__p = ONS_connection(env_, this_);
    
    t_minson_array *r = onsGetCategory(__p, category);
    
    (*env_)->ReleaseStringUTFChars(env_, category_, category);
    
    if(r == NULL)
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/offnet_storage/Connection$NotFoundException");
        (*env_)->ThrowNew(env_, exception, "Category not found!");
        return NULL;
    }
    return MinSON_instance(env_, minsonDuplicate(minsonUpcast(r)), true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_offnet_1storage_Connection_getCategoryAll
    (JNIEnv *env_, jobject this_, jstring category_)
{
    char const *category = (*env_)->GetStringUTFChars(env_, category_, NULL);
    if(category == NULL) return NULL;
    base_ons_connection *__p = ONS_connection(env_, this_);
    
    t_minson_array *r = onsGetCategoryAll(__p, category);
    
    (*env_)->ReleaseStringUTFChars(env_, category_, category);
    
    if(r == NULL)
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/offnet_storage/Connection$NotFoundException");
        (*env_)->ThrowNew(env_, exception, "Category not found!");
        return NULL;
    }
    return MinSON_instance(env_, minsonDuplicate(minsonUpcast(r)), true);
}

JNIEXPORT jstring JNICALL Java_bred_1soft_offnet_1storage_Connection_generate
    (JNIEnv *env_, jobject this_, jstring category_)
{
    char const *category = (*env_)->GetStringUTFChars(env_, category_, NULL);
    if(category == NULL) return NULL;
    base_ons_connection *__p = ONS_connection(env_, this_);
    
    char const *r = onsGenerateEntry(__p, category);
    
    (*env_)->ReleaseStringUTFChars(env_, category_, category);
    
    if(r == NULL)
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/offnet_storage/Connection$NotFoundException");
        (*env_)->ThrowNew(env_, exception, "Category not found!");
        return NULL;
    }
    return (*env_)->NewStringUTF(env_, r);
}

// ---

JNIEXPORT jobject JNICALL Java_bred_1soft_offnet_1storage_Connection_get
    (JNIEnv *env_, jobject this_, jstring category_, jstring entry_)
{
    char const *category = (*env_)->GetStringUTFChars(env_, category_, NULL);
    char const *entry = (*env_)->GetStringUTFChars(env_, entry_, NULL);
    if(category == NULL || entry == NULL)
    {
        if(category != NULL) (*env_)->ReleaseStringUTFChars(env_, category_, category);
        if(entry != NULL) (*env_)->ReleaseStringUTFChars(env_, entry_, entry);
        return NULL;
    }
    base_ons_connection *__p = ONS_connection(env_, this_);
    
    t_minson_object *r = onsGetEntry(__p, category, entry);
    
    (*env_)->ReleaseStringUTFChars(env_, category_, category);
    (*env_)->ReleaseStringUTFChars(env_, entry_, entry);
    
    if(r == NULL)
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/offnet_storage/Connection$NotFoundException");
        (*env_)->ThrowNew(env_, exception, "Entry not found!");
        return NULL;
    }
    return MinSON_instance(env_, minsonDuplicate(r), true);
}

JNIEXPORT jboolean JNICALL Java_bred_1soft_offnet_1storage_Connection_set
    (JNIEnv *env_, jobject this_, jstring category_, jobject entry_)
{
    if(entry_ == NULL) return 0;
    char const *category = (*env_)->GetStringUTFChars(env_, category_, NULL);
    if(category == NULL) return 0;
    t_minson_object *entry = MinSON_obj(env_, entry_);
    base_ons_connection *__p = ONS_connection(env_, this_);
    
    jboolean r = onsSetEntry(__p, category, entry);
    
    (*env_)->ReleaseStringUTFChars(env_, category_, category);
    return r;
}

JNIEXPORT jboolean JNICALL Java_bred_1soft_offnet_1storage_Connection_remove
    (JNIEnv *env_, jobject this_, jstring category_, jstring entry_)
{
    char const *category = (*env_)->GetStringUTFChars(env_, category_, NULL);
    char const *entry = (*env_)->GetStringUTFChars(env_, entry_, NULL);
    if(category == NULL || entry == NULL)
    {
        if(category != NULL) (*env_)->ReleaseStringUTFChars(env_, category_, category);
        if(entry != NULL) (*env_)->ReleaseStringUTFChars(env_, entry_, entry);
        return 0;
    }
    base_ons_connection *__p = ONS_connection(env_, this_);
    
    jboolean r = onsDeleteEntry(__p, category, entry);
    
    (*env_)->ReleaseStringUTFChars(env_, category_, category);
    (*env_)->ReleaseStringUTFChars(env_, entry_, entry);
    return r;
}

// ---

JNIEXPORT jboolean JNICALL Java_bred_1soft_offnet_1storage_Connection_flush
    (JNIEnv *env_, jobject this_)
{
    base_ons_connection *__p = ONS_connection(env_, this_);
    return onsFlush(__p);
}

JNIEXPORT jboolean JNICALL Java_bred_1soft_offnet_1storage_Connection_reset
    (JNIEnv *env_, jobject this_)
{
    base_ons_connection *__p = ONS_connection(env_, this_);
    return onsReset(__p);
}

// ---

JNIEXPORT jboolean JNICALL Java_bred_1soft_offnet_1storage_Connection_changeMemberName
    (JNIEnv *env_, jobject this_, jstring new_name_)
{
    char const *new_name = (*env_)->GetStringUTFChars(env_, new_name_, NULL);
    if(new_name == NULL) return 0;
    base_ons_connection *__p = ONS_connection(env_, this_);
    
    jboolean r = onsChangeMemberName(__p, new_name);
    
    (*env_)->ReleaseStringUTFChars(env_, new_name_, new_name);
    return r;
}

// ---

JNIEXPORT jboolean JNICALL Java_bred_1soft_offnet_1storage_Connection_synchronization
    (JNIEnv *env_, jclass class_, jobject c1_, jobject c2_)
{
    if(c1_ == NULL || c2_ == NULL) return 0;
    base_ons_connection *__p1 = ONS_connection(env_, c1_);
    base_ons_connection *__p2 = ONS_connection(env_, c2_);
    
    return onsSynchronization(__p1, __p2);
}
