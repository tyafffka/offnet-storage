#include "m.h"
#include "_core_impl.h"
#include "_core_utils.h"
#include "offnet-storage/config.h"

#define abs(a) ((a >= 0)? a : -a)


char *__get_member_name(t_ons_context *this_)
{
    if(this_->_core->__sync_context != NULL)
        return _generate_member_name();
    
    t_minson_object *m_object =
        _load(this_->_core->_storage_directory, NULL, ONS_DATA_MATRIX);
    t_minson_array *matrix = minsonCastArray(m_object);
    
    char *self_name = NULL;
    if(matrix != NULL) for(t_minson_iter it = minsonFirst(matrix);
                           minsonNotEnd(it); minsonNext(it))
    {
        if(minsonIterGetArray(it) != NULL)
        {
            self_name = m_new(char, strlen(minsonIterName(it)) + 1);
            strcpy(self_name, minsonIterName(it));
            break;
        }
    }
    
    minsonDelete(m_object);
    return (self_name != NULL)? self_name : _generate_member_name();
}

// --- *** ---


uint8_t /*bool*/ _change_member_name(t_ons_context *this_, // блокирующий
                                     char const *new_name_)
{
    if(new_name_ == NULL) return 0;
    
/* { */ _lock(&(this_->_core->__mutex));
    
    if(this_->_core->__sync_context != NULL)
    {
        /* } */ _unlock(&(this_->_core->__mutex)); return 0;
    }
    
    t_minson_object *m_object =
        _load(this_->_core->_storage_directory, NULL, ONS_DATA_MATRIX);
    t_minson_array *matrix = minsonCastArray(m_object);
    
    if(matrix == NULL)
    {
        minsonDelete(m_object);
        m_object = minsonUpcast(matrix = minsonCreateArray(NULL));
        minsonArrayAddArray(matrix, new_name_);
    }
    else if(minsonArrayGet(matrix, new_name_) != NULL)
    {
        /* } */ _unlock(&(this_->_core->__mutex));
        minsonDelete(m_object);
        return 0;
    }
    else
    {
        t_minson_array *self_row = NULL;
        for(t_minson_iter it = minsonFirst(matrix); minsonNotEnd(it); minsonNext(it))
        {
            self_row = minsonIterGetArray(it);
            if(self_row != NULL)
            {
                (void)minsonIterRemove(matrix, it); break;
            }
        }
        
        t_minson_array *new_self_row = minsonCreateArray(new_name_);
        minsonArrayAdd(matrix, minsonUpcast(new_self_row));
        
        if(self_row != NULL)
        {
            for(t_minson_iter it = minsonFirst(self_row); minsonNotEnd(it);)
            {
                t_minson_iter itpre = it; minsonNext(it);
                minsonArrayAdd(new_self_row, minsonIterRemove(self_row, itpre));
            }
            
            minsonArrayAddNumber(matrix, self_row->h.name, -_current_timestamp());
            minsonDeleteArray(self_row);
        }
    }
    
    _save(this_->_core->_storage_directory, NULL, ONS_DATA_MATRIX, m_object);
    
/* } */ _unlock(&(this_->_core->__mutex));
    
    minsonDelete(m_object);
    return 1;
}


t_minson_array *_get_matrix_impl(t_ons_context *this_) // блокирующий
{
/* { */ _lock(&(this_->_core->__mutex));
    
    if(this_->_core->__sync_context == NULL)
    {   this_->_core->__sync_context = this_;   }
    else if(this_->_core->__sync_context != this_)
    {
        /* } */ _unlock(&(this_->_core->__mutex)); return NULL;
    }
    
    t_minson_object *object = _load(this_->_core->_storage_directory,
                                    NULL, ONS_DATA_MATRIX);
    t_minson_array *matrix = minsonCastArray(object);
    
    if(matrix == NULL)
    {
        if(object != NULL)
        {
            /* } */ _unlock(&(this_->_core->__mutex));
            minsonDelete(object);
            return NULL;
        }
        
        char *new_name = _generate_member_name();
        matrix = minsonCreateArray(NULL);
        minsonArrayAddArray(matrix, new_name);
        m_delete(new_name);
        
        _save(this_->_core->_storage_directory, NULL, ONS_DATA_MATRIX,
              minsonUpcast(matrix));
    }
    
/* } */ _unlock(&(this_->_core->__mutex));
    return matrix;
}


uint8_t /*bool*/ _set_matrix_impl(t_ons_context *this_, // блокирующий
                                  t_minson_array *matrix_)
{
    if(matrix_ == NULL) return 0;
    
    char const *old_name = matrix_->h.name;
    matrix_->h.name = NULL;
    
/* { */ _lock(&(this_->_core->__mutex));
    
    uint8_t r = 0;
    if(this_->_core->__sync_context == this_)
    {
        _save(this_->_core->_storage_directory, NULL, ONS_DATA_MATRIX,
              minsonUpcast(matrix_));
        r = 1;
    }
    
/* } */ _unlock(&(this_->_core->__mutex));
    
    matrix_->h.name = old_name;
    return r;
}


t_minson_array *_get_table_impl(t_ons_context *this_) // блокирующий
{
/* { */ _lock(&(this_->_core->__mutex));
    
    t_minson_array *table = NULL;
    
    if(this_->_core->__sync_context == NULL)
        this_->_core->__sync_context = this_;
    else if(this_->_core->__sync_context != this_)
        goto __end;
    
    table = minsonDuplicateArray(this_->_core->_table);
    
__end:
/* } */ _unlock(&(this_->_core->__mutex));
    return table;
}


uint8_t /*bool*/ _set_time_impl(t_ons_context *this_, // блокирующий
                                char const *category_, char const *entry_,
                                int64_t time_)
{
    if(category_ == NULL || entry_ == NULL || time_ < 0) return 0;
    
/* { */ _lock(&(this_->_core->__mutex));
    
    uint8_t r = 0;
    if(this_->_core->__sync_context != this_) goto __end;
    
    t_minson_array *category = minsonArrayGetArray(this_->_journal_times, category_);
    if(category != NULL)
    {
        t_minson_object *entry = minsonArrayRemove(category, entry_);
        if(entry != NULL)
        {
            int64_t cur_time_;
            if(! minsonGetNumber(entry, &cur_time_)) cur_time_ = 0;
            minsonDelete(entry);
            
            minsonArrayAddNumber(category, entry_, ((cur_time_ < 0)? -time_ : time_));
            r = 1;
            goto __end;
        }
    }
    
    category = minsonArrayGetArray(this_->_core->_table, category_);
    if(category != NULL)
    {
        t_minson_object *entry = minsonArrayRemove(category, entry_);
        if(entry != NULL)
        {
            int64_t cur_time_;
            if(! minsonGetNumber(entry, &cur_time_)) cur_time_ = 0;
            minsonDelete(entry);
            
            minsonArrayAddNumber(category, entry_, ((cur_time_ < 0)? -time_ : time_));
            r = 1;
        }
    }
    
__end:
/* } */ _unlock(&(this_->_core->__mutex));
    return r;
}


uint8_t /*bool*/ _hard_delete_impl(t_ons_context *this_, // блокирующий
                                   char const *category_, char const *entry_,
                                   int64_t horizon_)
{
    if(category_ == NULL || entry_ == NULL) return 0;
    
/* { */ _lock(&(this_->_core->__mutex));
    
    uint8_t r = 0;
    if(this_->_core->__sync_context != this_) goto __end;
    
    t_minson_array *category = minsonArrayGetArray(this_->_core->_table, category_);
    if(category != NULL)
    {
        int64_t cur_time_;
        if(! minsonArrayGetNumber(category, entry_, &cur_time_)) goto __end;
        if(abs(cur_time_) > horizon_) goto __end;
        
        t_minson_object *entry = minsonArrayRemove(category, entry_);
        if(entry != NULL)
        {
            minsonDelete(entry); r = 1;
        }
    }
    
    category = minsonArrayGetArray(this_->_core->_fast_stored, category_);
    if(category != NULL)
        minsonDelete(minsonArrayRemove(category, entry_));
    else
        _delete(this_->_core->_storage_directory, category_, entry_);
    
__end:
/* } */ _unlock(&(this_->_core->__mutex));
    return r;
}


#undef abs
