#ifndef _OFFNET_STORAGE_HPP
#define _OFFNET_STORAGE_HPP

#include <offnet-storage/interface.h>
#include <minson.hpp>
#include <string>

namespace ons
{


/* Исключение, генерируемое, когда встречается пустая строка или имя. */
class string_error {};
/* Исключение, генерируемое при возникновении ошибкок при инициализации. */
class url_error {};
/* Исключение, генерируемое, если запрошенные данные не найдены или не доступны. */
class not_found_exception {};


/* Класс соединения, через которое вызываются все методы работы с хранилищем. */
class connection
{
    friend bool synchronization(connection const &, connection const &);
    
    connection(connection const &cp) = delete;
    connection &operator =(connection const &cp) = delete;
    connection &operator =(connection &&mv) = delete;
    
private:
    base_ons_connection *__p;
    
    explicit connection(base_ons_connection *p_)
    {   __p = p_;   }
    
public:
    explicit connection(std::string const &url_)
        /*throw(string_error, url_error)*/
    {
        if(url_.empty()) throw string_error{};
        __p = onsCreateConnection(url_.c_str(), 1 /*check api*/);
        if(! __p) throw url_error{};
    }
    connection(connection &&mv)
    {
        __p = mv.__p; mv.__p = nullptr;
    }
    ~connection()
    {
        if(__p) onsDeleteConnection(__p);
    }
    
    minson::array getCategory(std::string const &category_)
        /*throw(string_error, not_found_exception)*/
    {
        if(category_.empty()) throw string_error{};
        t_minson_array *r = onsGetCategory(__p, category_.c_str());
        if(! r) throw not_found_exception{};
        return minson::array{r};
    }
    minson::array getCategoryAll(std::string const &category_)
        /*throw(string_error, not_found_exception)*/
    {
        if(category_.empty()) throw string_error{};
        t_minson_array *r = onsGetCategoryAll(__p, category_.c_str());
        if(! r) throw not_found_exception{};
        return minson::array{r};
    }
    
    std::string generate(std::string const &category_)
        /*throw(string_error, not_found_exception)*/
    {
        if(category_.empty()) throw string_error{};
        char const *r = onsGenerateEntry(__p, category_.c_str());
        if(! r) throw not_found_exception{};
        return std::string{r};
    }
    
    minson::object get(std::string const &category_, std::string const &entry_)
        /*throw(string_error, not_found_exception)*/
    {
        if(category_.empty() || entry_.empty()) throw string_error{};
        t_minson_object *r = onsGetEntry(__p, category_.c_str(), entry_.c_str());
        if(! r) throw not_found_exception{};
        return minson::object{r};
    }
    bool set(std::string const &category_, minson::object const &entry_)
        /*throw(string_error)*/
    {
        if(category_.empty() || ! entry_.hasName()) throw string_error{};
        return(onsSetEntry(__p, category_.c_str(), entry_.__get_obj()) != 0);
    }
    bool remove(std::string const &category_, std::string const &entry_)
        /*throw(string_error)*/
    {
        if(category_.empty() || entry_.empty()) throw string_error{};
        return(onsDeleteEntry(__p, category_.c_str(), entry_.c_str()) != 0);
    }
    
    bool flush()
    {   return(onsFlush(__p) != 0);   }
    bool reset()
    {   return(onsReset(__p) != 0);   }
    
    bool changeMemberName(std::string const &new_name_)
    {
        if(new_name_.empty()) throw string_error{};
        return(onsChangeMemberName(__p, new_name_.c_str()) != 0);
    }
}; // class connection


/* Процедура синхронизации двух хранилищ
 * (информация об алгоритме [здесь](doc/synchronization.md)). */
inline bool synchronization(connection const &c1_, connection const &c2_)
{   return(onsSynchronization(c1_.__p, c2_.__p) != 0);   }


} // namespace ons

#endif // _OFFNET_STORAGE_HPP
