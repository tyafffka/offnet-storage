#ifndef __CONNECTION_PY_H
#define __CONNECTION_PY_H

#include "m.h"
#include <offnet-storage.h>
#include <Python.h>
#include <structmember.h>
#include <stdbool.h>

/* -*- Copied from minson-py -*- */

struct __t_MinSONObject
{
    PyObject_HEAD
    t_minson_object *__obj;
    bool __is_own;
};

#define pobj(self) (m_pcast(struct __t_MinSONObject *, self)->__obj)


extern PyObject *_minson_instance(t_minson_object *obj_, bool is_own_);

extern PyTypeObject MinSONObjectClass;

// --- *** ---


struct __t_Connection
{
    PyObject_HEAD
    base_ons_connection *__p;
};

#define pconn(self) m_pcast(struct __t_Connection *, self)


extern PyObject *ons_URLError;

extern PyTypeObject ConnectionClass;


#endif // __CONNECTION_PY_H
