#ifndef _OFFNET_STORAGE_PROTOCOLS_H
#define _OFFNET_STORAGE_PROTOCOLS_H

#include <minson.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


/* Базовый объект соединения, через который вызываются
 * все методы работы с хранилищем.
 * 
 * - `v_adapter` - адаптер отвечает за доставку команд и ответов между
 *   клиентом и сервером; объект запроса принадлежит вызывающей функции,
 *   объект ответа - соединению. */
typedef struct base_ons_connection
{
    void (*v_destructor)(struct base_ons_connection *this_);
    t_minson_object *(*v_adapter)(struct base_ons_connection *this_,
                                  t_minson_object *request_);
    t_minson_object *__last_response;
}
base_ons_connection;

typedef base_ons_connection *(*t_ons_connection_constructor)
    (char const *params_);

/* Разбирает URL и создаёт на его основе объект соединения.
 * Полученный объект следует удалять с помощью `onsDeleteConnection`. */
base_ons_connection *onsCreateConnection(char const *url_,
                                         uint8_t /*bool*/ check_api_);
/* Закрывает соединение и освобождает память объекта,
 * полученного ранее с помощью функции `onsCreateConnection`. */
void onsDeleteConnection(base_ons_connection *this_);

/* Отправляет сформированный запрос `request_` по соединению и возвращает
 * полученный ответ. Объект ответа удалять не нужно. */
t_minson_object *onsConnectionAdapter(base_ons_connection *this_,
                                      t_minson_object *request_);
// --- *** ---


struct base_ons_client;


/* Базовый объект сервера. Через сервер следует
 * принимать все новые соединения от клиентов.
 * 
 * - `v_new_client` - принять новое соединение и создать объект клиента. */
typedef struct base_ons_server
{
    void (*v_destructor)(struct base_ons_server *this_);
    struct base_ons_client *(*v_new_client)(struct base_ons_server *this_);
}
base_ons_server;

typedef base_ons_server *(*t_ons_server_constructor)
    (t_minson_array *params_);

/* Создаёт объект сервера типа `protocol_` на основе MinSON-массива
 * параметров `params_`. Подробнее о параметрах [здесь](doc/files.md).
 * Полученный объект следует удалять с помощью функции `onsDeleteServer`. */
base_ons_server *onsCreateServer(char const *protocol_,
                                 t_minson_array *params_);
/* Деинициализирует и освобождает память объекта сервера,
 * полученного ранее с помощью функции `onsCreateServer`.
 * 
 * _Внимание_: объекты клиентов следует удалять отдельно! */
void onsDeleteServer(base_ons_server *this_);

/* Проверяет, есть ли новое соединение, принимает его, создаёт и возвращает
 * объект клиента, который следует удалять с помощью `onsDeleteClient`.
 * Если новых соединений нет, возвращает `NULL`. */
#define onsNewClient(this_) (((this_)->v_new_client)((this_)))

// --- *** ---


/* Объект клиента на сервере.
 * 
 * - `v_request` - принять запрос от клиента, если есть.
 * - `v_response` - отправить ответ клиенту. */
typedef struct base_ons_client
{
    void (*v_destructor)(struct base_ons_client *this_);
    uint8_t /*bool*/ (*v_request)(struct base_ons_client *this_,
                                  t_minson_object **out_rq_);
    uint8_t /*bool*/ (*v_response)(struct base_ons_client *this_,
                                   t_minson_object *rs_);
}
base_ons_client;

/* Удалить ранее созданный объект клиента. */
void onsDeleteClient(base_ons_client *this_);

/* Принимает один запрос от данного клиента и возвращает объект запроса
 * в переменной по адресу `out_rq_`. Если новых запросов нет, записывает
 * `NULL` туда же. Объект запроса следует удалять с помощью `minsonDelete`.
 * Возвращает ненулевое значение в случае успеха и ноль в случае ошибки.
 * Если операция неуспешна, объект клиента следует удалить. */
#define onsClientRequest(this_, out_rq_) (((this_)->v_request)((this_), (out_rq_)))
/* Отправить клиенту ответ `rs_`. Объект ответа принадлежит вызывающему.
 * Возвращает ненулевое значение в случае успеха и ноль в случае ошибки.
 * Если операция неуспешна, объект клиента следует удалить. */
#define onsClientResponse(this_, rs_) (((this_)->v_response)((this_), (rs_)))

// --- *** ---


/* Добавить новый протокол с именем `protocol_` (не должно быть пустым).
 * Если протокол с таким именем уже существует, новый не будет добавлен.
 * 
 * - `connection_constructor_` - функция создания объекта соединения;
 * - `server_constructor_` - функция создания объекта сервера. */
void onsRegisterProtocol(char const *protocol_,
                         t_ons_connection_constructor connection_constructor_,
                         t_ons_server_constructor server_constructor_);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _OFFNET_STORAGE_PROTOCOLS_H
