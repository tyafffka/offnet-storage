#include "m.h"
#include "_core_impl.h"
#include "_core_utils.h"
#include "offnet-storage/config.h"

#define abs(a) ((a >= 0)? a : -a)


t_minson_array *_get_category_impl(t_ons_context *this_, // блокирующий
                                   char const *category_)
{
    if(category_ == NULL) return NULL;
    
    t_minson_array *array = minsonCreateArray(NULL);
    
/* { */ _lock(&(this_->_core->__mutex));
    
    t_minson_array *category = minsonArrayGetArray(this_->_core->_table, category_);
    if(category != NULL)
    {
        for(t_minson_iter it = minsonFirst(category); minsonNotEnd(it); minsonNext(it))
        {
            if(minsonIterName(it) == NULL) continue;
            
            int64_t time__;
            if(! minsonIterGetNumber(it, &time__)) continue;
            
            if(time__ >= 0)
                minsonArrayAddNumber(array, minsonIterName(it), time__);
        }
    }
    
/* } */ _unlock(&(this_->_core->__mutex));
    return array;
}


t_minson_array *_get_category_all_impl(t_ons_context *this_, // блокирующий
                                       char const *category_)
{
    if(category_ == NULL) return NULL;
    
    t_minson_array *array = minsonCreateArray(NULL);
    
/* { */ _lock(&(this_->_core->__mutex));
    
    t_minson_array *category = minsonArrayGetArray(this_->_core->_table, category_);
    if(category != NULL)
    {
        for(t_minson_iter it = minsonFirst(category); minsonNotEnd(it); minsonNext(it))
        {
            if(minsonIterName(it) == NULL) continue;
            
            int64_t time__;
            if(! minsonIterGetNumber(it, &time__)) continue;
            
            minsonArrayAddNumber(array, minsonIterName(it), time__);
        }
    }
    
/* } */ _unlock(&(this_->_core->__mutex));
    return array;
}


char const *_generate_entry_impl(t_ons_context *this_, // блокирующий
                                 char const *category_)
{
    if(category_ == NULL) return NULL;
    
/* { */ _lock(&(this_->_core->__mutex));
    
    t_minson_array *category = minsonArrayGetArray(this_->_core->_table, category_);
    if(category == NULL)
    {
        category = minsonCreateArray(category_);
        minsonArrayAdd(this_->_core->_table, minsonUpcast(category));
    }
    
    char *tmp_name = __get_member_name(this_);
    size_t name_length = strlen(tmp_name);
    
    char gen_name[name_length + 1 /*hyphen*/ + sizeof(int64_t) * 2 /*hex length*/ + 1];
    memcpy(gen_name, tmp_name, name_length);
    m_delete(tmp_name);
    
    int64_t curr_time = _current_timestamp();
    for(int64_t gen_time = curr_time;; ++gen_time)
    {
        sprintf(gen_name + name_length, "-%016lx", gen_time);
        
        int64_t time__;
        if(! minsonArrayGetNumber(category, gen_name, &time__)) break;
        if(time__ < 0) break;
    }
    
    t_minson_object *table_time = minsonCreateNumber(gen_name, curr_time);
    minsonDelete(minsonArrayRemove(category, gen_name));
    minsonArrayAdd(category, table_time);
    
    t_minson_object *obj = minsonCreateNull(gen_name);
    
    t_minson_array *fast_category = minsonArrayGetArray(this_->_core->_fast_stored, category_);
    if(fast_category != NULL)
    {
        minsonDelete(minsonArrayGet(fast_category, gen_name));
        minsonArrayAdd(fast_category, obj);
    }
    else
    {
        _save(this_->_core->_storage_directory, category_, gen_name, obj);
        minsonDelete(obj);
    }
    
/* } */ _unlock(&(this_->_core->__mutex));
    return table_time->name;
}


t_minson_object *_get_entry_impl(t_ons_context *this_, // полублокирующий
                                 char const *category_, char const *entry_)
{
    if(category_ == NULL || entry_ == NULL) return NULL;
    
    // lookup in journal (short way)
    
    t_minson_array *category = minsonArrayGetArray(this_->_journal_times, category_);
    if(category != NULL)
    {
        int64_t time__;
        if(minsonArrayGetNumber(category, entry_, &time__))
        {
            if(time__ < 0) return NULL;
            
            category = minsonArrayGetArray(this_->_journal, category_);
            if(category == NULL) return NULL;
            
            return minsonDuplicate(minsonArrayGet(category, entry_));
        }
    }
    
    t_minson_object *entry = NULL;
    
/* { */ _lock(&(this_->_core->__mutex));
    
    // check by Table
    
    category = minsonArrayGetArray(this_->_core->_table, category_);
    if(category == NULL) goto __alter_end;
    
    int64_t time__;
    if(! minsonArrayGetNumber(category, entry_, &time__)) goto __alter_end;
    if(time__ < 0) goto __alter_end;
    
    // lookup in fast-stored
    
    category = minsonArrayGetArray(this_->_core->_fast_stored, category_);
    if(category != NULL)
    {
        entry = minsonArrayGet(category, entry_);
        if(entry != NULL)
        {
            entry = minsonDuplicate(entry); goto __alter_end;
        }
    }
    
/* } */ _unlock(&(this_->_core->__mutex));
    
    // read from disk
    return _load(this_->_core->_storage_directory, category_, entry_);
    
__alter_end:
/* } */ _unlock(&(this_->_core->__mutex));
    return entry;
}


uint8_t /*bool*/ _set_entry_impl(t_ons_context *this_, // неблокирующий
                                 char const *category_, t_minson_object *entry_)
{
    if(category_ == NULL || entry_ == NULL) return 0;
    if(entry_->name == NULL) return 0;
    
    t_minson_array *category = minsonArrayGetArray(this_->_journal_times, category_);
    if(category == NULL)
    {
        category = minsonCreateArray(category_);
        minsonArrayAdd(this_->_journal_times, minsonUpcast(category));
    }
    
    minsonDelete(minsonArrayRemove(category, entry_->name));
    minsonArrayAddNumber(category, entry_->name, _current_timestamp());
    
    category = minsonArrayGetArray(this_->_journal, category_);
    if(category == NULL)
    {
        category = minsonCreateArray(category_);
        minsonArrayAdd(this_->_journal, minsonUpcast(category));
    }
    
    minsonDelete(minsonArrayRemove(category, entry_->name));
    minsonArrayAdd(category, minsonDuplicate(entry_));
    return 1;
}


uint8_t /*bool*/ _delete_entry_impl(t_ons_context *this_, // неблокирующий
                                    char const *category_, char const *entry_)
{
    if(category_ == NULL || entry_ == NULL) return 0;
    
    t_minson_array *category = minsonArrayGetArray(this_->_journal_times, category_);
    if(category == NULL)
    {
        category = minsonCreateArray(category_);
        minsonArrayAdd(this_->_journal_times, minsonUpcast(category));
    }
    
    minsonDelete(minsonArrayRemove(category, entry_));
    minsonArrayAddNumber(category, entry_, -_current_timestamp());
    return 1;
}


uint8_t /*bool*/ _flush_impl(t_ons_context *this_) // блокирующий
{
/* { */ _lock(&(this_->_core->__mutex));
    
    // for categories in journal
    
    uint8_t /*bool*/ is_table_touched = 0;
    
    for(t_minson_iter it = minsonFirst(this_->_journal_times);
        minsonNotEnd(it); minsonNext(it))
    {
        char const *category_name = minsonIterName(it);
        if(category_name == NULL) continue;
        
        t_minson_array *category = minsonIterGetArray(it);
        if(category == NULL) continue;
        
        t_minson_array *journal_category =
            minsonArrayGetArray(this_->_journal, category_name);
        
        t_minson_array *table_category =
            minsonArrayGetArray(this_->_core->_table, category_name);
        if(table_category == NULL)
        {
            table_category = minsonCreateArray(category_name);
            minsonArrayAdd(this_->_core->_table, minsonUpcast(table_category));
        }
        
        t_minson_array *fast_category =
            minsonArrayGetArray(this_->_core->_fast_stored, category_name);
        uint8_t /*bool*/ is_fast_touched = 0;
        
        // for entries in journal
        
        for(t_minson_iter it2 = minsonFirst(category);
            minsonNotEnd(it2); minsonNext(it2))
        {
            char const *entry_name = minsonIterName(it2);
            if(entry_name == NULL) continue;
            
            int64_t journal_time;
            if(! minsonIterGetNumber(it2, &journal_time)) continue;
            
            int64_t table_time = 0;
            t_minson_object *table_time__ = minsonArrayGet(table_category, entry_name);
            
            if(table_time__ != NULL) if(! minsonGetNumber(table_time__, &table_time))
            {
                minsonDelete(minsonArrayRemove(table_category, entry_name));
                table_time__ = NULL;
            }
            if(table_time__ == NULL)
            {
                table_time = 0;
                table_time__ = minsonCreateNumber(entry_name, table_time);
                minsonArrayAdd(table_category, table_time__);
            }
            
            if(abs(journal_time) >= abs(table_time))
            {
                if(journal_time > 0)
                {
                    if(journal_category == NULL) continue;
                    
                    t_minson_object *journal_entry =
                        minsonArrayRemove(journal_category, entry_name);
                    if(journal_entry == NULL) continue;
                    
                    if(fast_category != NULL)
                    {
                        minsonDelete(minsonArrayRemove(fast_category, entry_name));
                        minsonArrayAdd(fast_category, journal_entry);
                        is_fast_touched = 1;
                    }
                    else
                    {
                        _save(this_->_core->_storage_directory,
                              category_name, entry_name, journal_entry);
                        minsonDelete(journal_entry);
                    }
                }
                
                m_pcast(t_minson_number *, table_time__)->value = journal_time;
                is_table_touched = 1;
            }
        } //- for(entries)
        
        if(is_fast_touched)
        {
            _save(this_->_core->_storage_directory, category_name, NULL,
                  minsonUpcast(fast_category));
        }
    } //- for(categories)
    
    if(is_table_touched)
    {
        _save(this_->_core->_storage_directory, NULL, ONS_DATA_TABLE,
              minsonUpcast(this_->_core->_table));
    }
    
    // Clean up:
    
    if(this_->_core->__sync_context == this_)
        this_->_core->__sync_context = NULL;
    
/* } */ _unlock(&(this_->_core->__mutex));
    
    minsonDeleteArray(this_->_journal_times);
    this_->_journal_times = minsonCreateArray(NULL);
    
    minsonDeleteArray(this_->_journal);
    this_->_journal = minsonCreateArray(NULL);
    return 1;
}


uint8_t /*bool*/ _reset_impl(t_ons_context *this_) // блокирующий
{
/* { */ _lock(&(this_->_core->__mutex));
    
    if(this_->_core->__sync_context == this_)
        this_->_core->__sync_context = NULL;
    
/* } */ _unlock(&(this_->_core->__mutex));
    
    minsonDeleteArray(this_->_journal_times);
    this_->_journal_times = minsonCreateArray(NULL);
    
    minsonDeleteArray(this_->_journal);
    this_->_journal = minsonCreateArray(NULL);
    return 1;
}


#undef abs
