#include "m.h"
#include "_socket.h"
#include "offnet-storage/config.h"
#include <unistd.h>
#include <netinet/in.h>
#ifndef _WIN32
# include <sys/un.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>


static void __socket_close(base_ons_connection *this_)
{
    _t_ons_socket_connection *this = m_pcast(_t_ons_socket_connection *, this_);
    
    if(this->_connection_fd >= 0) close(this->_connection_fd);
    m_delete(this);
    return;
}


static t_minson_object *__socket_adapter(base_ons_connection *this_,
                                         t_minson_object *request_)
{
    _t_ons_socket_connection *this = m_pcast(_t_ons_socket_connection *, this_);
    
    t_minson_string_builder *builder = NULL;
    char *transfer_buffer = NULL;
    t_minson_object *response = NULL;
    
    // ===>
    
    builder = minsonCreateStringBuilder();
    if(! minsonSerialize(request_, builder))
        goto __end;
    size_t length = minsonStringBuilderSize(builder);
    
    transfer_buffer = m_new(char, length);
    minsonBuildString(builder, transfer_buffer);
    minsonDeleteStringBuilder(builder); builder = NULL;
    
    uint32_t n_length = htonl((uint32_t)length);
    if(write(this->_connection_fd, (void *)&n_length, sizeof(uint32_t)) != sizeof(uint32_t))
        goto __end;
    fsync(this->_connection_fd);
    
    if(write(this->_connection_fd, (void *)transfer_buffer, length) != length)
        goto __end;
    m_delete(transfer_buffer); transfer_buffer = NULL;
    fsync(this->_connection_fd);
    
    // <===
    
    if(read(this->_connection_fd, (void *)&n_length, sizeof(uint32_t)) != sizeof(uint32_t))
        goto __end;
    length = (size_t)ntohl(n_length);
    
    transfer_buffer = m_new(char, length);
    if(read(this->_connection_fd, (void *)transfer_buffer, length) != length)
        goto __end;
    response = minsonParse2(transfer_buffer, transfer_buffer + length);
    
__end:
    if(builder != NULL) minsonDeleteStringBuilder(builder);
    if(transfer_buffer != NULL) m_delete(transfer_buffer);
    return (response != NULL)? response : minsonCreateNull(NULL);
}


base_ons_connection *_onsCreateTCP(char const *params_)
{
    unsigned int octets[4], port;
    
    if(sscanf(params_, "%u.%u.%u.%u:%u",
              octets, octets+1, octets+2, octets+3, &port) != 5)
    {   return NULL;   }
    
    if(octets[0] > 0xFF || octets[1] > 0xFF ||
       octets[2] > 0xFF || octets[3] > 0xFF || port > 0xFFFF)
    {   return NULL;   }
    
    uint32_t ip = htonl((octets[0] << 24) | (octets[1] << 16) |
                        (octets[2] << 8) | octets[3]);
    
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0) return NULL;
    
    struct sockaddr_in addr; memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = ip;
    addr.sin_port = htons((uint16_t)port);
    
    if(connect(fd, m_pcast(struct sockaddr *, &addr), sizeof(addr)) != 0)
    {
        close(fd); return NULL;
    }
    
    _t_ons_socket_connection *this = m_new(_t_ons_socket_connection, 1);
    this->h.v_destructor = __socket_close;
    this->h.v_adapter = __socket_adapter;
    this->_connection_fd = fd;
    return m_pcast(base_ons_connection *, this);
}


#ifndef _WIN32
base_ons_connection *_onsCreateUDS(char const *params_)
{
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(fd < 0) return NULL;
    
    struct sockaddr_un addr; memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, params_, sizeof(addr.sun_path) - 1);
    
    if(connect(fd, m_pcast(struct sockaddr *, &addr), sizeof(addr)) != 0)
    {
        close(fd); return NULL;
    }
    
    _t_ons_socket_connection *this = m_new(_t_ons_socket_connection, 1);
    this->h.v_destructor = __socket_close;
    this->h.v_adapter = __socket_adapter;
    this->_connection_fd = fd;
    return m_pcast(base_ons_connection *, this);
}
#endif

// --- *** ---


static void __socket_close_server(base_ons_server *this_)
{
    _t_ons_socket_server *this = m_pcast(_t_ons_socket_server *, this_);
    
    if(this->_server_fd >= 0) close(this->_server_fd);
    m_delete(this);
    return;
}


static base_ons_client *__socket_create_client(int fd_, uint32_t ip_, uint16_t port_);


static base_ons_client *__socket_new_client(base_ons_server *this_)
{
    _t_ons_socket_server *this = m_pcast(_t_ons_socket_server *, this_);
    
    fd_set rset; FD_ZERO(&rset); FD_SET(this->_server_fd, &rset);
    
static struct timeval tv = (struct timeval){0, 0};
    if(select(this->_server_fd + 1, &rset, NULL, NULL, &tv) <= 0)
    {   return NULL;   }
    
    // Create client
    
    struct sockaddr addr;
    socklen_t addrlen = sizeof(addr);
    int fd = accept(this->_server_fd, &addr, &addrlen);
    if(fd < 0) return NULL;
    
    if(addr.sa_family == AF_INET)
    {
        return __socket_create_client(fd,
            m_cast(struct sockaddr_in, addr).sin_addr.s_addr,
            ntohs(m_cast(struct sockaddr_in, addr).sin_port)
        );
    }
    else
    {   return __socket_create_client(fd, 0, 0);   }
}


base_ons_server *_onsCreateTCPServer(t_minson_array *params_)
{
    char const *s_ip;
    unsigned int octets[4];
    int64_t port;
    int64_t max_connections;
    
    if((s_ip = minsonArrayGetString(params_, "ip")) == NULL)
    {   return NULL;   }
    if(! minsonArrayGetNumber(params_, "port", &port))
    {   return NULL;   }
    if(! minsonArrayGetNumber(params_, "max-connections", &max_connections))
    {   max_connections = 10;   }
    
    if(sscanf(s_ip, "%u.%u.%u.%u",
              octets, octets+1, octets+2, octets+3) != 4)
    {   return NULL;   }
    
    if(octets[0] > 0xFF || octets[1] > 0xFF ||
       octets[2] > 0xFF || octets[3] > 0xFF ||
       port > 0xFFFF || port < 0)
    {   return NULL;   }
    
    uint32_t ip = htonl((octets[0] << 24) | (octets[1] << 16) |
                        (octets[2] << 8) | octets[3]);
    
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0) return NULL;
    
    struct sockaddr_in addr; memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = ip;
    addr.sin_port = htons((uint16_t)port);
    
    if(bind(fd, m_pcast(struct sockaddr *, &addr), sizeof(addr)) != 0)
    {
        close(fd); return NULL;
    }
    if(listen(fd, (int)max_connections) != 0)
    {
        close(fd); return NULL;
    }
    
    _t_ons_socket_server *this = m_new(_t_ons_socket_server, 1);
    this->h.v_destructor = __socket_close_server;
    this->h.v_new_client = __socket_new_client;
    this->_server_fd = fd;
    return m_pcast(base_ons_server *, this);
}


#ifndef _WIN32
base_ons_server *_onsCreateUDSServer(t_minson_array *params_)
{
    char const *path;
    int64_t max_connections;
    
    if((path = minsonArrayGetString(params_, "socket-file")) == NULL)
    {   return NULL;   }
    if(! minsonArrayGetNumber(params_, "max-connections", &max_connections))
    {   max_connections = 10;   }
    
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(fd < 0) return NULL;
    
    unlink(path);
    
    struct sockaddr_un addr; memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);
    
    if(bind(fd, m_pcast(struct sockaddr *, &addr), sizeof(addr)) != 0)
    {
        close(fd); return NULL;
    }
    if(listen(fd, (int)max_connections) != 0)
    {
        close(fd); return NULL;
    }
    
    chmod(path, 0666);
    
    _t_ons_socket_server *this = m_new(_t_ons_socket_server, 1);
    this->h.v_destructor = __socket_close_server;
    this->h.v_new_client = __socket_new_client;
    this->_server_fd = fd;
    return m_pcast(base_ons_server *, this);
}
#endif

// --- *** ---


static void __socket_close_client(base_ons_client *this_)
{
    _t_ons_socket_client *this = m_pcast(_t_ons_socket_client *, this_);
    
    if(this->_client_fd >= 0) close(this->_client_fd);
    m_delete(this);
    return;
}


static uint8_t /*bool*/ __socket_request(base_ons_client *this_,
                                         t_minson_object **out_rq_)
{
    _t_ons_socket_client *this = m_pcast(_t_ons_socket_client *, this_);
    
    fd_set rset; FD_ZERO(&rset); FD_SET(this->_client_fd, &rset);
    
struct timeval tv = (struct timeval){0, 0};
    if(select(this->_client_fd + 1, &rset, NULL, NULL, &tv) <= 0)
    {
        *out_rq_ = NULL; return 1;
    }
    
    // Get request
    
    uint32_t n_length;
    if(read(this->_client_fd, (void *)&n_length, sizeof(uint32_t)) != sizeof(uint32_t))
    {   return 0;   }
    size_t length = (int32_t)ntohl(n_length);
    
    char *transfer_buffer = m_new(char, length);
    if(read(this->_client_fd, (void *)transfer_buffer, length) != length)
    {
        m_delete(transfer_buffer); return 0;
    }
    
    *out_rq_ = minsonParse2(transfer_buffer, transfer_buffer + length);
    m_delete(transfer_buffer);
    
    return (*out_rq_ != NULL)? (uint8_t)1 : (uint8_t)0;
}


static uint8_t /*bool*/ __socket_response(base_ons_client *this_,
                                          t_minson_object *rs_)
{
    _t_ons_socket_client *this = m_pcast(_t_ons_socket_client *, this_);
    
    t_minson_string_builder *builder = minsonCreateStringBuilder();
    if(! minsonSerialize(rs_, builder))
    {
        minsonDeleteStringBuilder(builder); return 0;
    }
    size_t length = minsonStringBuilderSize(builder);
    
    char *transfer_buffer = m_new(char, length);
    minsonBuildString(builder, transfer_buffer);
    minsonDeleteStringBuilder(builder);
    
    uint32_t n_length = htonl((uint32_t)length);
    if(write(this->_client_fd, (void *)&n_length, sizeof(uint32_t)) != sizeof(uint32_t))
    {
        m_delete(transfer_buffer); return 0;
    }
    fsync(this->_client_fd);
    
    if(write(this->_client_fd, (void *)transfer_buffer, (size_t)length) != (size_t)length)
    {
        m_delete(transfer_buffer); return 0;
    }
    fsync(this->_client_fd);
    return 1;
}


base_ons_client *__socket_create_client(int fd_, uint32_t ip_, uint16_t port_)
{
    _t_ons_socket_client *this = m_new(_t_ons_socket_client, 1);
    this->h.v_destructor = __socket_close_client;
    this->h.v_request = __socket_request;
    this->h.v_response = __socket_response;
    this->_client_fd = fd_;
    this->ip = ip_;
    this->port = port_;
    return m_pcast(base_ons_client *, this);
}
