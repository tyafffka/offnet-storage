Программный интерфейс библиотеки — Java
---------------------------------------

_Пакет: `bred_soft.offnet_storage`_

--------------------------------

### `class Connection;`

Класс соединения, через которое вызываются все методы работы с хранилищем.

#### `static class URLError;`

_Исключение_. Генерируется, когда возникли ошибки при инициализации.

#### `static class NotFoundException;`

_Исключение_. Генерируется, если запрошенные данные не найдены или не
доступны.

#### `Connection(String url_);`

_Конструктор_. Создаёт соединение, используя параметры, указаные в URL.
Подробнее — [здесь](../files.md).

#### `MinSONArray getCategory(String category_);`

Возвращает массив записей запрошенной категории (время последнего изменения в
качестве значения).

#### `MinSONArray getCategoryAll(String category_);`

Возвращает массив всех записей категории, включая удалённые записи. Для
удалённых время последнего изменения указывается со знаком минус.

#### `String generate(String category_);`

Создать новую запись в указанной категории с произвольным именем. Возвращает
имя записи.

#### `MinSONObject get(String category_, String entry_);`

Возвращает MinSON-объект, сохранённый в указанной записи.

#### `boolean set(String category_, MinSONObject entry_);`

Сохранить MinSON-объект как запись в указанной категории. Объект должен быть
именованным. Возвращает успешность операции.

#### `boolean remove(String category_, String entry_);`

Удаляет указанную запись. Возвращает успешность операции.

#### `boolean flush();`

Зафиксировать (синхронизировать) изменения в хранилище.

#### `boolean reset();`

Отменить последние изменения и восстановить актуальное состояние хранилища.

#### `boolean changeMemberName(String new_name_);`

Изменяет имя абонента хранилища и правит матрицу актуальности соответствующим
образом. Возвращает успешность операции.

#### `static boolean synchronization(Connection c1_, Connection c2_);`

Процедура синхронизации двух хранилищ (информация об алгоритме
[здесь](../synchronization.md)). Возвращает успешность операции.
