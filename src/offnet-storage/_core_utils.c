#ifdef _GNU_SOURCE
# define _GNU_SOURCE_END 0
#else
# define _GNU_SOURCE
# define _GNU_SOURCE_END 1
#endif /* { */

#include "m.h"
#include "_core_utils.h"
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/file.h>
#ifdef _WIN32
# include <windows.h>
#endif

#if _GNU_SOURCE_END /* } */
# undef _GNU_SOURCE
#endif

#define __MEMBER_NAME_MAX 255


static char *__new_path(char const *base_dir_, char const *category_,
                        char const *entry_)
{
    size_t base_length = strlen(base_dir_);
    size_t category_length = (category_ != NULL)?(strlen(category_) + 1): 0;
    size_t entry_length = (entry_ != NULL)? strlen(entry_) : 0;
    
    char *path = m_new(char, base_length + category_length + entry_length + 1);
    strcpy(path, base_dir_);
    
    if(category_length > 0)
    {
        strcpy(path + base_length, category_);
        *(path + base_length + category_length - 1) = ':';
    }
    if(entry_length > 0)
    {
        strcpy(path + base_length + category_length, entry_);
    }
    return path;
}

// --- *** ---


char *_dirdup(char const *dir_)
{
    if(dir_ == NULL) return NULL;
    size_t dir_length = strlen(dir_);
    if(dir_length == 0) return NULL;
    
    uint8_t need_slash = 0;
    if(dir_[dir_length - 1] != '/' && dir_[dir_length - 1] != '\\')
        need_slash = 1;
    
    char *copy_dir = m_new(char, dir_length + need_slash + 1);
    strcpy(copy_dir, dir_);
    
    if(need_slash) strcpy(copy_dir + dir_length, "/");
    return copy_dir;
}


int64_t _current_timestamp()
{
#ifdef _WIN32
static unsigned __int64 const __epoch = (unsigned __int64)116444736000000000ULL;
    
    SYSTEMTIME st; GetSystemTime(&st);
    FILETIME ft; SystemTimeToFileTime(&st, &ft);
    
    ULARGE_INTEGER uft;
    uft.LowPart = ft.dwLowDateTime;
    uft.HighPart = ft.dwHighDateTime;
    
    register uint64_t timestamp = (uint64_t)(
        ((uft.QuadPart - __epoch) / 10000L) + st.wMilliseconds
    );
#else
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    
    register uint64_t timestamp =
        (uint64_t)ts.tv_sec * __UINT64_C(1000) +
        (uint64_t)ts.tv_nsec / __UINT64_C(1000000);
#endif
    return (int64_t)(timestamp & ~(__UINT64_C(1) << 63));
}


char *_generate_member_name()
{
    char *member_name = m_new(char, __MEMBER_NAME_MAX + 1);
#ifdef _WIN32
    long host_name_len = __MEMBER_NAME_MAX - 5;
    GetComputerName(member_name, &host_name_len);
#else
    gethostname(member_name, __MEMBER_NAME_MAX - 5);
    long host_name_len = strlen(member_name);
#endif
    
    srand((unsigned)(rand() ^ clock()));
    sprintf(member_name + host_name_len, "-%04x", (rand() & 0xFFFF));
    return member_name;
}

// --- *** ---


void _lock(atomic_flag volatile *mutex_)
{
    while(atomic_flag_test_and_set(mutex_))
    {
static struct timespec const __mutex_lock_loop_delay = (struct timespec const){0, 1};
        nanosleep(&__mutex_lock_loop_delay, NULL);
    }
    return;
}


void _unlock(atomic_flag volatile *mutex_)
{
    atomic_flag_clear(mutex_); return;
}


int /*nix*/ _file_lock(char const *base_dir_, char const *file_)
{
    char *path = __new_path(base_dir_, NULL, file_);
#ifdef _WIN32
    int fd = _open(path, _O_CREAT | _O_EXCL | _O_RDWR, _S_IREAD | _S_IWRITE);
#else
    int fd = open(path, O_CREAT | O_RDWR | O_TRUNC, 0644);
#endif
    m_delete(path);
    
    if(fd < 0) return -1;
    
#ifndef _WIN32
    if(flock(fd, LOCK_EX | LOCK_NB) < 0)
    {
        close(fd); return -2;
    }
#endif
    
    char pid_s[12];
    sprintf(pid_s, "%d\n", getpid());
    
    size_t pid_s_len = strlen(pid_s);
    if(write(fd, pid_s, pid_s_len) != pid_s_len)
    {
        close(fd); return -3;
    }
#ifdef _WIN32
    _commit(fd);
#else
    fsync(fd);
#endif
    return fd;
}


void _file_unlock(int fd_, char const *base_dir_, char const *file_)
{
    if(fd_ < 0) return;
    
    _delete(base_dir_, NULL, file_);
    close(fd_);
    return;
}

// --- *** ---


t_minson_object *_load(char const *base_dir_, char const *category_,
                       char const *entry_)
{
    char *path = __new_path(base_dir_, category_, entry_);
    t_minson_object *object = minsonParseFile(path);
    
    m_delete(path);
    return object;
}


void _save(char const *base_dir_, char const *category_,
           char const *entry_, t_minson_object *object_)
{
    char *path = __new_path(base_dir_, category_, entry_);
    size_t pathlen = strlen(path);
    
    char backpath[pathlen + 2];
    strcpy(backpath, path);
    strcpy(backpath + pathlen, "~");
    
    if(minsonSerializeFile(object_, backpath))
    {
        unlink(path);
        rename(backpath, path);
    }
    
    m_delete(path);
    return;
}


void _delete(char const *base_dir_, char const *category_,
             char const *entry_)
{
    char *path = __new_path(base_dir_, category_, entry_);
    unlink(path);
    
    m_delete(path);
    return;
}
