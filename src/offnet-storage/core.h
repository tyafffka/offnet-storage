#ifndef _OFFNET_STORAGE_CORE_H
#define _OFFNET_STORAGE_CORE_H

#include <minson.h>
#include <stdint.h>

#ifndef __cplusplus
# include <stdatomic.h>

typedef atomic_flag volatile _t_ons_mutex;
#else
# include <atomic>

typedef std::atomic_flag volatile _t_ons_mutex;

extern "C" {
#endif


struct t_ons_context;


/* Объект реализации хранилища - ядро сервера, должен быть создан первым
 * (управление памятью остаётся за Вами). Для каждого подключающегося клиента
 * необходимо создавать отдельный контекст, через который и происходит
 * обработка запросов.
 * 
 * _Внимание:_ все объекты контекста, связанные с данным объектом, должны быть
 * закрыты _ДО_ закрытия самого объекта реализации! */
typedef struct t_ons_core
{
    char const *_storage_directory;
    t_minson_array *_table; // { <category>{ <entry> (<time>) ... } ... }
    t_minson_array *_fast_stored; // { <category>{ <entry> <value> ... } ... }
    
    struct t_ons_context *__sync_context;
    int __lock_fd;
    _t_ons_mutex __mutex;
}
t_ons_core;

/* Конструктор объекта реализации. Принимает в качестве
 * аргумента путь к папке, где должны содержаться все данные
 * хранилища (папка должна существовать). */
uint8_t /*bool*/ onsCoreInit(t_ons_core *this_, char const *storage_dir_);
/* Деструктор объекта реализации. Убедитесь в том,
 * что все связанные объекты контекста закрыты. */
void onsCoreClose(t_ons_core *this_);

// --- *** ---


/* Объект контекста взаимодействия, через который происходит обработка
 * запросов от клиента. При инициализации связывается с объектом реализации
 * и должен быть закрыт _ДО_ закрытия этого объекта реализации. */
typedef struct t_ons_context
{
    t_ons_core *_core;
    t_minson_array *_journal_times; // { <category>{ <entry> (<time>) ... } ... }
    t_minson_array *_journal; // { <category>{ <entry> <value> ... } ... }
}
t_ons_context;

/* Базовый конструктор объекта контекста;
 * объект реализации в качестве аргумента. */
uint8_t /*bool*/ onsContextInit(t_ons_context *this_, t_ons_core *core_);
/* Базовый деструктор объекта контекста. */
void onsContextClose(t_ons_context *this_);

/* Обработать один запрос от клиента. Возвращает ответ, который необходимо
 * передать клиенту. Объект ответа следует удалять с помощью `minsonDelete`.
 * Вызовы в рамках одного контекста должны выполняться синхронно. */
t_minson_object *onsExecute(t_ons_context *this_, t_minson_object *request_);


#ifdef __cplusplus
} // extern "C"
#endif

#endif // _OFFNET_STORAGE_CORE_H
