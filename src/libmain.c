#include "offnet-storage/protocols.h"
#include "protocols/_direct.h"
#include "protocols/_proxy.h"
#include "protocols/_socket.h"


void _onsClearProtocols();


__attribute__((constructor)) void _onsInit( void )
{
    onsRegisterProtocol("direct", _onsCreateDirect, NULL);
    onsRegisterProtocol("proxy", _onsCreateProxy, NULL);
    onsRegisterProtocol("tcp", _onsCreateTCP, _onsCreateTCPServer);
#ifndef _WIN32
    onsRegisterProtocol("unix", _onsCreateUDS, _onsCreateUDSServer);
#endif
    return;
}


__attribute__((destructor)) void _onsClose( void )
{
    _onsClearProtocols(); return;
}
