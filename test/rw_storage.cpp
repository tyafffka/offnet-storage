#include "offnet-storage.hpp"
#include "test.h"
#include <time.h>


static char const *__write_example_data = "qwertyuiop --- ===";


int main(int argc, char *const *argv)
{
    _TEST_(argc == 2);
    try
    {
        // Open storage
        ons::connection exampeStorage = ons::connection(argv[1]);
        
        // Check name - it's not necessary.
        //std::string member_name = strrchr(argv[1], '/') + 1;
        //_TEST_(member_name == exampeStorage.memberName());
        
        // Generate entry
        std::string new_name = exampeStorage.generate("generated");
        _TEST_(exampeStorage.set(
            "generated", minson::object::string(__write_example_data, new_name)
        ));
        _TEST_(exampeStorage.flush());
        
        minson::array cat = exampeStorage.getCategory("generated");
        _TEST_(cat.count() > 0);
        
        // Iterate
        for(auto it = cat.begin(), itend = cat.end(); it != itend; ++it)
        {
            time_t entry_time = ((int64_t)*it) / 1000;
            minson::object entry = exampeStorage.get("generated", it.name());
            
            struct tm *tm__ = localtime(&entry_time);
            printf("generated:%s - %d.%02d.%d %d:%02d:%02d\n",
                   minson::serialize(entry).c_str(),
                   tm__->tm_mday, (tm__->tm_mon + 1), (tm__->tm_year + 1900),
                   tm__->tm_hour, tm__->tm_min, tm__->tm_sec);
            
            if(entry.name() == new_name)
            {
                _TEST_((std::string)entry == __write_example_data);
            }
        }
    }
    catch(ons::url_error &)
    {
        _FAIL_("wrong URL!");
    }
    catch(ons::not_found_exception &)
    {
        _FAIL_("category/entry not found!");
    }
    catch(minson::cast_error &)
    {
        _FAIL_("wrong data types!");
    }
    _ENDTEST_;
}
