#include "m.h"
#include "interface.h"
#include "_sync_interface.h"
#include "_core_utils.h"
#ifdef _WIN32
# include <windows.h>
#endif

#define abs(a) ((a >= 0)? a : -a)


static void __clean_self_list(t_minson_array *self_list_, t_minson_array *clean_from_)
{
    for(t_minson_iter it = minsonFirst(clean_from_); minsonNotEnd(it); minsonNext(it))
    {
        if(minsonIterName(it) == NULL) continue;
        
        int64_t time__;
        if(! minsonIterGetNumber(it, &time__)) continue;
        if(time__ >= 0) continue;
        
        int64_t time_to__;
        if(! minsonArrayGetNumber(self_list_, minsonIterName(it), &time_to__))
            continue;
        if(time_to__ >= -time__) continue;
        
        minsonDelete(minsonArrayRemove(self_list_, minsonIterName(it)));
    }
    return;
}


static int64_t __update_array(t_minson_array *array_to_, char const *name_to_,
                              t_minson_array *array_from_, char const *name_from_,
                              int64_t selftime_)
{
    minsonDelete(minsonArrayRemove(array_to_, name_from_));
    minsonArrayAddNumber(array_to_, name_from_, selftime_);
    
    for(t_minson_iter it = minsonFirst(array_from_); minsonNotEnd(it); minsonNext(it))
    {
        if(minsonIterName(it) == NULL) continue;
        if(strcmp(name_to_, minsonIterName(it)) == 0) continue;
        if(strcmp(name_from_, minsonIterName(it)) == 0) continue;
        
        int64_t time__;
        if(! minsonIterGetNumber(it, &time__)) continue;
        
        int64_t time_to__;
        if(minsonArrayGetNumber(array_to_, minsonIterName(it), &time_to__))
            if(abs(time_to__) >= abs(time__)) continue;
        
        minsonDelete(minsonArrayRemove(array_to_, minsonIterName(it)));
        minsonArrayAddNumber(array_to_, minsonIterName(it), time__);
    }
    
    int64_t min_time = selftime_;
    for(t_minson_iter it = minsonFirst(array_to_); minsonNotEnd(it); minsonNext(it))
    {
        int64_t time__;
        if(! minsonIterGetNumber(it, &time__)) continue;
        if(time__ >= 0 && time__ < min_time) min_time = time__;
    }
    return min_time;
}


static void __remove_old_times(t_minson_array *matrix_, int64_t time_border_)
{
    for(t_minson_iter it = minsonFirst(matrix_); minsonNotEnd(it);)
    {
        t_minson_iter itpre = it; minsonNext(it);
        
        int64_t time__;
        if(! minsonIterGetNumber(itpre, &time__)) continue;
        
        if(time__ < 0 && -time__ <= time_border_)
            minsonDelete(minsonIterRemove(matrix_, itpre));
    }
    return;
}


static uint8_t /*bool*/ __update_matrices(base_ons_connection *storage1_,
                                          base_ons_connection *storage2_,
                                          int64_t *out_horizon_)
{
    uint8_t r = 0;
    
    t_minson_array *matrix1 = NULL, *matrix2 = NULL;
    t_minson_array *self_list1 = NULL, *self_list2 = NULL;
    
    // Get matrix 1:
    
    if(( matrix1 = minsonDuplicateArray(_get_matrix(storage1_)) ) == NULL)
        goto __clear;
    for(t_minson_iter it = minsonFirst(matrix1); minsonNotEnd(it); minsonNext(it))
    {
        self_list1 = minsonIterGetArray(it);
        if(self_list1 != NULL) break;
    }
    if(self_list1 == NULL) goto __clear;
    
    // Get matrix 2:
    
    if(( matrix2 = minsonDuplicateArray(_get_matrix(storage2_)) ) == NULL)
        goto __clear;
    for(t_minson_iter it = minsonFirst(matrix2); minsonNotEnd(it); minsonNext(it))
    {
        self_list2 = minsonIterGetArray(it);
        if(self_list2 != NULL) break;
    }
    if(self_list2 == NULL) goto __clear;
    
    // Cross update:
    
    __clean_self_list(self_list1, matrix2);
    __clean_self_list(self_list2, matrix1);
    
    int64_t current_time = _current_timestamp();
    int64_t point1 = __update_array(self_list1, self_list1->h.name,
                                    self_list2, self_list2->h.name, current_time);
    int64_t point2 = __update_array(self_list2, self_list2->h.name,
                                    self_list1, self_list1->h.name, current_time);
    if(point1 != point2) goto __clear;
    
    point2 = __update_array(matrix1, self_list1->h.name,
                            matrix2, self_list2->h.name, point2);
    point1 = __update_array(matrix2, self_list2->h.name,
                            matrix1, self_list1->h.name, point1);
    if(point1 != point2) goto __clear;
    
    // Apply horizon:
    
    if(out_horizon_ != NULL) *out_horizon_ = point1;
    
    __remove_old_times(matrix1, point1);
    __remove_old_times(matrix2, point2);
    
    // Save matrices:
    
    r = 1;
    if(! _set_matrix(storage1_, matrix1)) r = 0;
    if(! _set_matrix(storage2_, matrix2)) r = 0;
    
__clear:
    minsonDeleteArray(matrix1); minsonDeleteArray(matrix2);
    return r;
}

// --- *** ---


static void __remove_old_entries(base_ons_connection *storage_,
                                 t_minson_array *table_, int64_t time_border_)
{
    for(t_minson_iter it = minsonFirst(table_); minsonNotEnd(it); minsonNext(it))
    {
        if(minsonIterName(it) == NULL) continue;
        t_minson_array *category = minsonIterGetArray(it);
        if(category == NULL) continue;
        
        for(t_minson_iter it2 = minsonFirst(category); minsonNotEnd(it2); minsonNext(it2))
        {
            if(minsonIterName(it2) == NULL) continue;
            int64_t time__;
            if(! minsonIterGetNumber(it2, &time__)) continue;
            
            if(time__ < 0 && -time__ <= time_border_)
            {
                _hard_delete(storage_, minsonIterName(it),
                             minsonIterName(it2), time_border_);
            }
        }
    }
    return;
}


static void __update_entries(base_ons_connection *to_storage_,
                             t_minson_array *to_table_,
                             base_ons_connection *from_storage_,
                             t_minson_array *from_table_, int64_t time_border_)
{
    for(t_minson_iter it = minsonFirst(from_table_); minsonNotEnd(it); minsonNext(it))
    {
        if(minsonIterName(it) == NULL) continue;
        t_minson_array *from_category = minsonIterGetArray(it);
        if(from_category == NULL) continue;
        
        t_minson_array *to_category = minsonArrayGetArray(
            to_table_, minsonIterName(it)
        );
        
        for(t_minson_iter it2 = minsonFirst(from_category); minsonNotEnd(it2); minsonNext(it2))
        {
            if(minsonIterName(it2) == NULL) continue;
            int64_t from_time__;
            if(! minsonIterGetNumber(it2, &from_time__)) continue;
            
            if(to_category != NULL)
            {
                int64_t to_time__;
                if(! minsonArrayGetNumber(to_category, minsonIterName(it2), &to_time__))
                    to_time__ = 0;
                
                if(abs(to_time__) >= abs(from_time__)) continue;
            }
            
            if(from_time__ >= 0)
            {
                t_minson_object *entry = onsGetEntry(
                    from_storage_, minsonIterName(it), minsonIterName(it2)
                );
                
                if(onsSetEntry(to_storage_, minsonIterName(it), entry))
                    _set_time(to_storage_, minsonIterName(it),
                              minsonIterName(it2), from_time__);
            }
            else if(-from_time__ <= time_border_)
            {
                _hard_delete(to_storage_, minsonIterName(it),
                             minsonIterName(it2), time_border_);
            }
            else
            {
                if(onsDeleteEntry(to_storage_, minsonIterName(it), minsonIterName(it2)))
                    _set_time(to_storage_, minsonIterName(it),
                              minsonIterName(it2), -from_time__);
            }
        } //- for(entry ...)
    } //- for(category ...)
    return;
}


#undef abs

// --- *** ---


uint8_t /*bool*/ onsSynchronization(base_ons_connection *storage1_,
                                    base_ons_connection *storage2_)
{
    if(storage1_ == NULL || storage2_ == NULL) return 0;
    
    // Слияние матриц актуальности
    int64_t relevance_horizon;
    uint8_t mr = __update_matrices(storage1_, storage2_, &relevance_horizon);
    
    t_minson_object *table1 = minsonDuplicate(_get_table(storage1_));
    if(table1 == NULL) goto __fail1;
    
    t_minson_object *table2 = minsonDuplicate(_get_table(storage2_));
    if(table2 == NULL) goto __fail2;
    
    if(mr) // Слились? Чистим удалённые записи.
    {
        __remove_old_entries(storage1_, minsonCastArray(table1), relevance_horizon);
        __remove_old_entries(storage2_, minsonCastArray(table2), relevance_horizon);
    }
    
    __update_entries(storage1_, minsonCastArray(table1),
                     storage2_, minsonCastArray(table2), relevance_horizon);
    __update_entries(storage2_, minsonCastArray(table2),
                     storage1_, minsonCastArray(table1), relevance_horizon);
    
    minsonDelete(table1); minsonDelete(table2);
    onsFlush(storage1_); onsFlush(storage2_);
    return 1;
    
__fail2:
    minsonDelete(table1);
__fail1:
    onsReset(storage1_); onsReset(storage2_);
    return 0;
}
