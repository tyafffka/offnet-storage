#include "m.h"
#include "_direct.h"


static void __base_direct_close(base_ons_connection *this_)
{
    _base_ons_direct_connection *this =
        m_pcast(_base_ons_direct_connection *, this_);
    
    onsContextClose(&(this->__context));
    m_delete(this);
    return;
}


static void __direct_close(base_ons_connection *this_)
{
    _t_ons_direct_connection *this =
        m_pcast(_t_ons_direct_connection *, this_);
    
    onsContextClose(&(this->h.__context));
    onsCoreClose(&(this->__core));
    m_delete(this);
    return;
}


static t_minson_object *__direct_adapter(base_ons_connection *this_,
                                         t_minson_object *request_)
{
    return onsExecute(
        &(m_pcast(_base_ons_direct_connection *, this_)->__context),
        request_
    );
}


base_ons_connection *_onsCreateBaseDirect(t_ons_core *core_)
{
    _base_ons_direct_connection *this = m_new(_base_ons_direct_connection, 1);
    
    this->h.v_destructor = __base_direct_close;
    this->h.v_adapter = __direct_adapter;
    this->h.__last_response = NULL;
    
    if(! onsContextInit(&(this->__context), core_))
    {
        m_delete(this); return NULL;
    }
    return m_pcast(base_ons_connection *, this);
}


base_ons_connection *_onsCreateDirect(char const *params_)
{
    _t_ons_direct_connection *this = m_new(_t_ons_direct_connection, 1);
    
    this->h.h.v_destructor = __direct_close;
    this->h.h.v_adapter = __direct_adapter;
    this->h.h.__last_response = NULL;
    
    if(! onsCoreInit(&(this->__core), params_))
    {
        m_delete(this); return NULL;
    }
    if(! onsContextInit(&(this->h.__context), &(this->__core)))
    {
        m_delete(this); return NULL;
    }
    return m_pcast(base_ons_connection *, this);
}
