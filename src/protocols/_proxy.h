#ifndef __OFFNET_STORAGE_PROXY_PROTOCOL_H
#define __OFFNET_STORAGE_PROXY_PROTOCOL_H

#include "offnet-storage/protocols.h"


base_ons_connection *_onsCreateProxy(char const *params_);


#endif // __OFFNET_STORAGE_PROXY_PROTOCOL_H
