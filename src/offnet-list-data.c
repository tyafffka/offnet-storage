#include "offnet-storage.h"
#include <string.h>
#include <stdio.h>


static int /*nix*/ ListData(char const *dir_)
{
    size_t dir_len = strlen(dir_);
    int need_slash = (dir_len <= 0)? 0 :
                     (dir_[dir_len - 1] != '/' && dir_[dir_len - 1] != '\\')? 1 : 0;
    
    char path_buffer[dir_len + need_slash + sizeof(ONS_DATA_TABLE) /*- 1 + 1*/];
    strcpy(path_buffer, dir_);
    if(need_slash) path_buffer[dir_len] = '/';
    dir_len += need_slash;
    
    // Read table
    
    strcpy(path_buffer + dir_len, ONS_DATA_TABLE);
    t_minson_object *table__ = minsonParseFile(path_buffer);
    t_minson_array *table = minsonCastArray(table__);
    
    if(table == NULL)
    {
        if(table__ != NULL) minsonDelete(table__);
        
        fprintf(stderr, "Error: can't load table in '%s'!\n", dir_);
        return 1;
    }
    fprintf(stdout, "%s\n", path_buffer);
    
    // Read fast stored
    
    strcpy(path_buffer + dir_len, ONS_DATA_FAST_STORED);
    t_minson_object *fast_stored__ = minsonParseFile(path_buffer);
    t_minson_array *fast_stored = minsonCastArray(fast_stored__);
    
    if(fast_stored != NULL)
    {
        fprintf(stdout, "%s\n", path_buffer);
    }
    else if(fast_stored__ != NULL)
    {
        fprintf(stderr, "Error: can't load fast-stored list in '%s'!\n", dir_);
        
        minsonDelete(fast_stored__);
        minsonDelete(table__);
        return 2;
    }
    
    path_buffer[dir_len] = '\0';
    for(t_minson_iter it = minsonFirst(table); minsonNotEnd(it); minsonNext(it))
    {
        char const *category = minsonIterName(it);
        if(category == NULL) continue;
        
        if(fast_stored != NULL) if(minsonArrayGet(fast_stored, category) != NULL)
        {
            fprintf(stdout, "%s%s:\n", path_buffer, category);
            continue;
        }
        
        t_minson_array *c_table = minsonIterGetArray(it);
        if(c_table == NULL) continue;
        
        for(t_minson_iter it2 = minsonFirst(c_table); minsonNotEnd(it2); minsonNext(it2))
        {
            char const *entry = minsonIterName(it2);
            if(entry == NULL) continue;
            
            fprintf(stdout, "%s%s:%s\n", path_buffer, category, entry);
        }
    }
    
    minsonDelete(fast_stored__);
    minsonDelete(table__);
    return 0;
}

// --- *** ---


int /*nix*/ main(int argc, char *const *argv)
{
    if(argc < 2)
    {
        fprintf(stderr,
"Usage: %s <storage-dir>\n"
"  <storage-dir> - path to storage for which will be listed files,\n"
"    needed to backup\n",
            argv[0]
        );
        return 0;
    }
    else
    {   return ListData(argv[1]);   }
}
